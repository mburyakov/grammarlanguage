<?xml version="1.0" encoding="UTF-8"?>
<solution name="jetbrains.mps.grammarLanguage.sandbox" uuid="e058338f-779b-40b4-b0a4-eccd643f5f56" moduleVersion="0" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
  </models>
  <sourcePath />
  <languageVersions>
    <language slang="l:ceab5195-25ea-4f22-9b92-103b95ca8c0c:jetbrains.mps.lang.core" version="2" />
    <language slang="l:89a8aaa6-0c50-49f2-a5c2-b956fa8e5588:jetbrains.mps.samples.grammarLanguage" version="0" />
  </languageVersions>
  <dependencyVersions>
    <module reference="e058338f-779b-40b4-b0a4-eccd643f5f56(jetbrains.mps.grammarLanguage.sandbox)" version="0" />
  </dependencyVersions>
</solution>

