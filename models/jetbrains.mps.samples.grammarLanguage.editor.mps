<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:93fe6cea-dd36-4bce-a060-2ca34a546afe(jetbrains.mps.samples.grammarLanguage.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="13" />
    <use id="602c36ad-cc55-47ff-8c40-73d7f12f035c" name="jetbrains.mps.lang.editor.forms" version="0" />
    <use id="0272d3b4-4cc8-481e-9e2f-07793fbfcb41" name="jetbrains.mps.lang.editor.table" version="0" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="c8s6" ref="r:08f8613a-9ff2-487d-bf55-5ceca2c577dd(jetbrains.mps.samples.grammarLanguage.structure)" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" />
    <import index="h9ya" ref="r:194bf60c-7dbf-4609-8b8d-8f296167a58e(jetbrains.mps.samples.grammarLanguage.behavior)" />
    <import index="33ny" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.util(JDK/)" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" implicit="true" />
    <import index="tpc2" ref="r:00000000-0000-4000-0000-011c8959029e(jetbrains.mps.lang.editor.structure)" implicit="true" />
    <import index="tpen" ref="r:00000000-0000-4000-0000-011c895902c3(jetbrains.mps.baseLanguage.editor)" implicit="true" />
  </imports>
  <registry>
    <language id="a247e09e-2435-45ba-b8d2-07e93feba96a" name="jetbrains.mps.baseLanguage.tuples">
      <concept id="1238852151516" name="jetbrains.mps.baseLanguage.tuples.structure.IndexedTupleType" flags="in" index="1LlUBW">
        <child id="1238852204892" name="componentType" index="1Lm7xW" />
      </concept>
      <concept id="1238853782547" name="jetbrains.mps.baseLanguage.tuples.structure.IndexedTupleLiteral" flags="nn" index="1Ls8ON">
        <child id="1238853845806" name="component" index="1Lso8e" />
      </concept>
      <concept id="1238857743184" name="jetbrains.mps.baseLanguage.tuples.structure.IndexedTupleMemberAccessExpression" flags="nn" index="1LFfDK">
        <child id="1238857764950" name="tuple" index="1LFl5Q" />
        <child id="1238857834412" name="index" index="1LF_Uc" />
      </concept>
    </language>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="2000375450116454183" name="jetbrains.mps.lang.editor.structure.ISubstituteMenu" flags="ng" index="22mbnS">
        <child id="414384289274416996" name="parts" index="3ft7WO" />
      </concept>
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi">
        <child id="1078153129734" name="inspectedCellModel" index="6VMZX" />
        <child id="2597348684684069742" name="contextHints" index="CpUAK" />
      </concept>
      <concept id="1597643335227097138" name="jetbrains.mps.lang.editor.structure.QueryFunctionParameter_TransformationMenu_node" flags="ng" index="7Obwk" />
      <concept id="6516520003787916624" name="jetbrains.mps.lang.editor.structure.QueryFunction_TransformationMenu_Condition" flags="ig" index="27VH4U" />
      <concept id="6822301196700715228" name="jetbrains.mps.lang.editor.structure.ConceptEditorHintDeclarationReference" flags="ig" index="2aJ2om">
        <reference id="5944657839026714445" name="hint" index="2$4xQ3" />
      </concept>
      <concept id="1140524381322" name="jetbrains.mps.lang.editor.structure.CellModel_ListWithRole" flags="ng" index="2czfm3">
        <property id="1140524450557" name="separatorText" index="2czwfO" />
        <child id="1140524464360" name="cellLayout" index="2czzBx" />
      </concept>
      <concept id="1196434649611" name="jetbrains.mps.lang.editor.structure.SubstituteMenu_SimpleString" flags="ng" index="2h3Zct">
        <property id="1196434851095" name="text" index="2h4Kg1" />
      </concept>
      <concept id="1106270549637" name="jetbrains.mps.lang.editor.structure.CellLayout_Horizontal" flags="nn" index="2iRfu4" />
      <concept id="1106270571710" name="jetbrains.mps.lang.editor.structure.CellLayout_Vertical" flags="nn" index="2iRkQZ" />
      <concept id="1237303669825" name="jetbrains.mps.lang.editor.structure.CellLayout_Indent" flags="nn" index="l2Vlx" />
      <concept id="1237307900041" name="jetbrains.mps.lang.editor.structure.IndentLayoutIndentStyleClassItem" flags="ln" index="lj46D" />
      <concept id="1237308012275" name="jetbrains.mps.lang.editor.structure.IndentLayoutNewLineStyleClassItem" flags="ln" index="ljvvj" />
      <concept id="1237375020029" name="jetbrains.mps.lang.editor.structure.IndentLayoutNewLineChildrenStyleClassItem" flags="ln" index="pj6Ft" />
      <concept id="1142886221719" name="jetbrains.mps.lang.editor.structure.QueryFunction_NodeCondition" flags="in" index="pkWqt" />
      <concept id="1142886811589" name="jetbrains.mps.lang.editor.structure.ConceptFunctionParameter_node" flags="nn" index="pncrf" />
      <concept id="1237385578942" name="jetbrains.mps.lang.editor.structure.IndentLayoutOnNewLineStyleClassItem" flags="ln" index="pVoyu" />
      <concept id="1177327274449" name="jetbrains.mps.lang.editor.structure.QueryFunctionParameter_pattern" flags="nn" index="ub8z3" />
      <concept id="1177327570013" name="jetbrains.mps.lang.editor.structure.QueryFunction_SubstituteMenu_Substitute" flags="in" index="ucgPf" />
      <concept id="8478191136883534237" name="jetbrains.mps.lang.editor.structure.IExtensibleSubstituteMenuPart" flags="ng" index="upBLQ">
        <child id="8478191136883534238" name="features" index="upBLP" />
      </concept>
      <concept id="4242538589859161874" name="jetbrains.mps.lang.editor.structure.ExplicitHintsSpecification" flags="ng" index="2w$q5c">
        <child id="4242538589859162459" name="hints" index="2w$qW5" />
      </concept>
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="6718020819487620876" name="jetbrains.mps.lang.editor.structure.TransformationMenuReference_Default" flags="ng" index="A1WHr" />
      <concept id="5944657839000868711" name="jetbrains.mps.lang.editor.structure.ConceptEditorContextHints" flags="ig" index="2ABfQD">
        <child id="5944657839000877563" name="hints" index="2ABdcP" />
      </concept>
      <concept id="5944657839003601246" name="jetbrains.mps.lang.editor.structure.ConceptEditorHintDeclaration" flags="ig" index="2BsEeg">
        <property id="168363875802087287" name="showInUI" index="2gpH_U" />
      </concept>
      <concept id="3473224453637651916" name="jetbrains.mps.lang.editor.structure.TransformationLocation_SideTransform_PlaceInCellHolder" flags="ng" index="CtIbL">
        <property id="3473224453637651917" name="placeInCell" index="CtIbK" />
      </concept>
      <concept id="1239814640496" name="jetbrains.mps.lang.editor.structure.CellLayout_VerticalGrid" flags="nn" index="2EHx9g" />
      <concept id="1638911550608571617" name="jetbrains.mps.lang.editor.structure.TransformationMenu_Default" flags="ng" index="IW6AY" />
      <concept id="1638911550608610798" name="jetbrains.mps.lang.editor.structure.QueryFunction_TransformationMenu_Execute" flags="ig" index="IWg2L" />
      <concept id="1638911550608610278" name="jetbrains.mps.lang.editor.structure.TransformationMenuPart_Action" flags="ng" index="IWgqT">
        <child id="6202297022026447496" name="canExecuteFunction" index="2jiSrf" />
        <child id="1638911550608610281" name="executeFunction" index="IWgqQ" />
        <child id="5692353713941573325" name="textFunction" index="1hCUd6" />
      </concept>
      <concept id="1186403694788" name="jetbrains.mps.lang.editor.structure.ColorStyleClassItem" flags="ln" index="VaVBg">
        <property id="1186403713874" name="color" index="Vb096" />
      </concept>
      <concept id="1186404549998" name="jetbrains.mps.lang.editor.structure.ForegroundColorStyleClassItem" flags="ln" index="VechU" />
      <concept id="1186414536763" name="jetbrains.mps.lang.editor.structure.BooleanStyleSheetItem" flags="ln" index="VOi$J">
        <property id="1186414551515" name="flag" index="VOm3f" />
      </concept>
      <concept id="1186414928363" name="jetbrains.mps.lang.editor.structure.SelectableStyleSheetItem" flags="ln" index="VPM3Z" />
      <concept id="1186414949600" name="jetbrains.mps.lang.editor.structure.AutoDeletableStyleClassItem" flags="ln" index="VPRnO" />
      <concept id="1186414976055" name="jetbrains.mps.lang.editor.structure.DrawBorderStyleClassItem" flags="ln" index="VPXOz" />
      <concept id="1630016958697718209" name="jetbrains.mps.lang.editor.structure.IMenuReference_Default" flags="ng" index="2Z_bC8">
        <reference id="1630016958698373342" name="concept" index="2ZyFGn" />
      </concept>
      <concept id="1630016958697344083" name="jetbrains.mps.lang.editor.structure.IMenu_Concept" flags="ng" index="2ZABuq">
        <reference id="6591946374543067572" name="conceptDeclaration" index="aqKnT" />
      </concept>
      <concept id="1630016958697286851" name="jetbrains.mps.lang.editor.structure.QueryFunctionParameter_parameterObject" flags="ng" index="2ZBlsa" />
      <concept id="1630016958697057551" name="jetbrains.mps.lang.editor.structure.IMenuPartParameterized" flags="ng" index="2ZBHr6">
        <child id="1630016958697057552" name="parameterType" index="2ZBHrp" />
      </concept>
      <concept id="1233758997495" name="jetbrains.mps.lang.editor.structure.PunctuationLeftStyleClassItem" flags="ln" index="11L4FC" />
      <concept id="8998492695583109601" name="jetbrains.mps.lang.editor.structure.QueryFunction_SubstituteMenu_CanSubstitute" flags="ig" index="16Na2f" />
      <concept id="8998492695583125082" name="jetbrains.mps.lang.editor.structure.SubstituteFeature_MatchingText" flags="ng" index="16NfWO">
        <child id="8998492695583129244" name="query" index="16NeZM" />
      </concept>
      <concept id="8998492695583129971" name="jetbrains.mps.lang.editor.structure.SubstituteFeature_DescriptionText" flags="ng" index="16NL0t">
        <child id="8998492695583129972" name="query" index="16NL0q" />
      </concept>
      <concept id="8998492695583129991" name="jetbrains.mps.lang.editor.structure.SubstituteFeature_CanSubstitute" flags="ng" index="16NL3D">
        <child id="8998492695583129992" name="query" index="16NL3A" />
      </concept>
      <concept id="1154465273778" name="jetbrains.mps.lang.editor.structure.QueryFunctionParameter_SubstituteMenu_ParentNode" flags="nn" index="3bvxqY" />
      <concept id="1838685759388685703" name="jetbrains.mps.lang.editor.structure.TransformationFeature_DescriptionText" flags="ng" index="3cqGtN">
        <child id="1838685759388685704" name="query" index="3cqGtW" />
      </concept>
      <concept id="1838685759388690401" name="jetbrains.mps.lang.editor.structure.QueryFunction_TransformationMenu_DescriptionText" flags="ig" index="3cqJkl" />
      <concept id="2896773699153795590" name="jetbrains.mps.lang.editor.structure.TransformationLocation_SideTransform" flags="ng" index="3cWJ9i">
        <child id="3473224453637651919" name="placeInCell" index="CtIbM" />
      </concept>
      <concept id="7342352913006985483" name="jetbrains.mps.lang.editor.structure.SubstituteMenuPart_Action" flags="ng" index="3eGOop">
        <child id="8612453216082699922" name="substituteHandler" index="3aKz83" />
      </concept>
      <concept id="5692353713941573329" name="jetbrains.mps.lang.editor.structure.QueryFunction_TransformationMenu_ActionLabelText" flags="ig" index="1hCUdq" />
      <concept id="1088013125922" name="jetbrains.mps.lang.editor.structure.CellModel_RefCell" flags="sg" stub="730538219795941030" index="1iCGBv">
        <child id="1088186146602" name="editorComponent" index="1sWHZn" />
      </concept>
      <concept id="7291101478617127464" name="jetbrains.mps.lang.editor.structure.IExtensibleTransformationMenuPart" flags="ng" index="1joUw2">
        <child id="8954657570916349207" name="features" index="2jZA2a" />
      </concept>
      <concept id="1381004262292414836" name="jetbrains.mps.lang.editor.structure.ICellStyle" flags="ng" index="1k5N5V">
        <reference id="1381004262292426837" name="parentStyleClass" index="1k5W1q" />
      </concept>
      <concept id="1236262245656" name="jetbrains.mps.lang.editor.structure.MatchingLabelStyleClassItem" flags="ln" index="3mYdg7">
        <property id="1238091709220" name="labelName" index="1413C4" />
      </concept>
      <concept id="3308396621974580100" name="jetbrains.mps.lang.editor.structure.SubstituteMenu_Default" flags="ng" index="3p36aQ" />
      <concept id="1088185857835" name="jetbrains.mps.lang.editor.structure.InlineEditorComponent" flags="ig" index="1sVBvm" />
      <concept id="5425882385312046132" name="jetbrains.mps.lang.editor.structure.QueryFunctionParameter_SubstituteMenu_CurrentTargetNode" flags="nn" index="1yR$tW" />
      <concept id="1215007762405" name="jetbrains.mps.lang.editor.structure.FloatStyleClassItem" flags="ln" index="3$6MrZ">
        <property id="1215007802031" name="value" index="3$6WeP" />
      </concept>
      <concept id="1215007897487" name="jetbrains.mps.lang.editor.structure.PaddingRightStyleClassItem" flags="ln" index="3$7jql" />
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <property id="1140017977771" name="readOnly" index="1Intyy" />
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389214265" name="jetbrains.mps.lang.editor.structure.EditorCellModel" flags="ng" index="3EYTF0">
        <child id="4202667662392416064" name="transformationMenu" index="3vIgyS" />
      </concept>
      <concept id="1073389446423" name="jetbrains.mps.lang.editor.structure.CellModel_Collection" flags="sn" stub="3013115976261988961" index="3EZMnI">
        <child id="1106270802874" name="cellLayout" index="2iSdaV" />
        <child id="1073389446424" name="childCellModel" index="3EZMnx" />
      </concept>
      <concept id="1073389577006" name="jetbrains.mps.lang.editor.structure.CellModel_Constant" flags="sn" stub="3610246225209162225" index="3F0ifn">
        <property id="1073389577007" name="text" index="3F0ifm" />
      </concept>
      <concept id="1073389658414" name="jetbrains.mps.lang.editor.structure.CellModel_Property" flags="sg" stub="730538219796134133" index="3F0A7n" />
      <concept id="1219418625346" name="jetbrains.mps.lang.editor.structure.IStyleContainer" flags="ng" index="3F0Thp">
        <child id="1219418656006" name="styleItem" index="3F10Kt" />
      </concept>
      <concept id="1073389882823" name="jetbrains.mps.lang.editor.structure.CellModel_RefNode" flags="sg" stub="730538219795960754" index="3F1sOY">
        <child id="5861024100072578575" name="addHints" index="3xwHhi" />
      </concept>
      <concept id="1073390211982" name="jetbrains.mps.lang.editor.structure.CellModel_RefNodeList" flags="sg" stub="2794558372793454595" index="3F2HdR" />
      <concept id="4233361609415247331" name="jetbrains.mps.lang.editor.structure.QueryFunction_TransformationMenu_Parameter" flags="ig" index="1GhMSn" />
      <concept id="4233361609415240997" name="jetbrains.mps.lang.editor.structure.TransformationMenuPart_Parameterized" flags="ng" index="1GhOrh">
        <child id="4233361609415240998" name="part" index="1GhOri" />
        <child id="4233361609415241000" name="parameterQuery" index="1GhOrs" />
      </concept>
      <concept id="1225898583838" name="jetbrains.mps.lang.editor.structure.ReadOnlyModelAccessor" flags="ng" index="1HfYo3">
        <child id="1225898971709" name="getter" index="1Hhtcw" />
      </concept>
      <concept id="1225900081164" name="jetbrains.mps.lang.editor.structure.CellModel_ReadOnlyModelAccessor" flags="sg" stub="3708815482283559694" index="1HlG4h">
        <child id="1225900141900" name="modelAccessor" index="1HlULh" />
      </concept>
      <concept id="5624877018228267058" name="jetbrains.mps.lang.editor.structure.ITransformationMenu" flags="ng" index="3INCJE">
        <child id="1638911550608572412" name="sections" index="IW6Ez" />
      </concept>
      <concept id="1088612959204" name="jetbrains.mps.lang.editor.structure.CellModel_Alternation" flags="sg" stub="8104358048506729361" index="1QoScp">
        <property id="1088613081987" name="vertical" index="1QpmdY" />
        <child id="1145918517974" name="alternationCondition" index="3e4ffs" />
        <child id="1088612958265" name="ifTrueCellModel" index="1QoS34" />
        <child id="1088612973955" name="ifFalseCellModel" index="1QoVPY" />
      </concept>
      <concept id="7980428675268276156" name="jetbrains.mps.lang.editor.structure.TransformationMenuSection" flags="ng" index="1Qtc8_">
        <child id="7980428675268276157" name="locations" index="1Qtc8$" />
        <child id="7980428675268276159" name="parts" index="1Qtc8A" />
      </concept>
      <concept id="1176717841777" name="jetbrains.mps.lang.editor.structure.QueryFunction_ModelAccess_Getter" flags="in" index="3TQlhw" />
      <concept id="2722384699544370949" name="jetbrains.mps.lang.editor.structure.SubstituteMenuPart_Placeholder" flags="ng" index="3VyMlK" />
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="4836112446988635817" name="jetbrains.mps.baseLanguage.structure.UndefinedType" flags="in" index="2jxLKc" />
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="1225271283259" name="jetbrains.mps.baseLanguage.structure.NPEEqualsExpression" flags="nn" index="17R0WA" />
      <concept id="1225271408483" name="jetbrains.mps.baseLanguage.structure.IsNotEmptyOperation" flags="nn" index="17RvpY" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242875" name="jetbrains.mps.baseLanguage.structure.PlusExpression" flags="nn" index="3cpWs3" />
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="6329021646629104954" name="jetbrains.mps.baseLanguage.structure.SingleLineComment" flags="nn" index="3SKdUt">
        <child id="1350122676458893092" name="text" index="3ndbpf" />
      </concept>
      <concept id="1080120340718" name="jetbrains.mps.baseLanguage.structure.AndExpression" flags="nn" index="1Wc70l" />
    </language>
    <language id="fd392034-7849-419d-9071-12563d152375" name="jetbrains.mps.baseLanguage.closures">
      <concept id="1199569711397" name="jetbrains.mps.baseLanguage.closures.structure.ClosureLiteral" flags="nn" index="1bVj0M">
        <child id="1199569906740" name="parameter" index="1bW2Oz" />
        <child id="1199569916463" name="body" index="1bW5cS" />
      </concept>
    </language>
    <language id="3a13115c-633c-4c5c-bbcc-75c4219e9555" name="jetbrains.mps.lang.quotation">
      <concept id="5455284157993911077" name="jetbrains.mps.lang.quotation.structure.NodeBuilderInitProperty" flags="ng" index="2pJxcG">
        <reference id="5455284157993911078" name="property" index="2pJxcJ" />
        <child id="1595412875168045201" name="initValue" index="28ntcv" />
      </concept>
      <concept id="5455284157993863837" name="jetbrains.mps.lang.quotation.structure.NodeBuilder" flags="nn" index="2pJPEk">
        <child id="5455284157993863838" name="quotedNode" index="2pJPEn" />
      </concept>
      <concept id="5455284157993863840" name="jetbrains.mps.lang.quotation.structure.NodeBuilderNode" flags="nn" index="2pJPED">
        <reference id="5455284157993910961" name="concept" index="2pJxaS" />
        <child id="5455284157993911099" name="values" index="2pJxcM" />
      </concept>
    </language>
    <language id="602c36ad-cc55-47ff-8c40-73d7f12f035c" name="jetbrains.mps.lang.editor.forms">
      <concept id="7024409093146622323" name="jetbrains.mps.lang.editor.forms.structure.CheckboxUI_Platform" flags="ng" index="jv8YD" />
      <concept id="312429380032619384" name="jetbrains.mps.lang.editor.forms.structure.CellModel_Checkbox" flags="ng" index="2yq9I_">
        <reference id="3696012239575138271" name="propertyDeclaration" index="225u1j" />
        <child id="1340057216891284122" name="ui" index="1563LE" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="4705942098322609812" name="jetbrains.mps.lang.smodel.structure.EnumMember_IsOperation" flags="ng" index="21noJN">
        <child id="4705942098322609813" name="member" index="21noJM" />
      </concept>
      <concept id="4705942098322467729" name="jetbrains.mps.lang.smodel.structure.EnumMemberReference" flags="ng" index="21nZrQ">
        <reference id="4705942098322467736" name="decl" index="21nZrZ" />
      </concept>
      <concept id="1179168000618" name="jetbrains.mps.lang.smodel.structure.Node_GetIndexInParentOperation" flags="nn" index="2bSWHS" />
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1138411891628" name="jetbrains.mps.lang.smodel.structure.SNodeOperation" flags="nn" index="eCIE_">
        <child id="1144104376918" name="parameter" index="1xVPHs" />
      </concept>
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="4693937538533521280" name="jetbrains.mps.lang.smodel.structure.OfConceptOperation" flags="ng" index="v3k3i">
        <child id="4693937538533538124" name="requestedConcept" index="v3oSu" />
      </concept>
      <concept id="1173122760281" name="jetbrains.mps.lang.smodel.structure.Node_GetAncestorsOperation" flags="nn" index="z$bX8" />
      <concept id="2396822768958367367" name="jetbrains.mps.lang.smodel.structure.AbstractTypeCastExpression" flags="nn" index="$5XWr">
        <child id="6733348108486823193" name="leftExpression" index="1m5AlR" />
        <child id="3906496115198199033" name="conceptArgument" index="3oSUPX" />
      </concept>
      <concept id="1171305280644" name="jetbrains.mps.lang.smodel.structure.Node_GetDescendantsOperation" flags="nn" index="2Rf3mk" />
      <concept id="1171310072040" name="jetbrains.mps.lang.smodel.structure.Node_GetContainingRootOperation" flags="nn" index="2Rxl7S" />
      <concept id="1145572800087" name="jetbrains.mps.lang.smodel.structure.Node_GetPrevSiblingsOperation" flags="nn" index="2Ttrtt" />
      <concept id="1171407110247" name="jetbrains.mps.lang.smodel.structure.Node_GetAncestorOperation" flags="nn" index="2Xjw5R" />
      <concept id="1143512015885" name="jetbrains.mps.lang.smodel.structure.Node_GetNextSiblingOperation" flags="nn" index="YCak7" />
      <concept id="3562215692195599741" name="jetbrains.mps.lang.smodel.structure.SLinkImplicitSelect" flags="nn" index="13MTOL">
        <reference id="3562215692195600259" name="link" index="13MTZf" />
      </concept>
      <concept id="2644386474300074836" name="jetbrains.mps.lang.smodel.structure.ConceptIdRefExpression" flags="nn" index="35c_gC">
        <reference id="2644386474300074837" name="conceptDeclaration" index="35c_gD" />
      </concept>
      <concept id="1139613262185" name="jetbrains.mps.lang.smodel.structure.Node_GetParentOperation" flags="nn" index="1mfA1w" />
      <concept id="1139621453865" name="jetbrains.mps.lang.smodel.structure.Node_IsInstanceOfOperation" flags="nn" index="1mIQ4w">
        <child id="1177027386292" name="conceptArgument" index="cj9EA" />
      </concept>
      <concept id="1171999116870" name="jetbrains.mps.lang.smodel.structure.Node_IsNullOperation" flags="nn" index="3w_OXm" />
      <concept id="1172008320231" name="jetbrains.mps.lang.smodel.structure.Node_IsNotNullOperation" flags="nn" index="3x8VRR" />
      <concept id="1144100932627" name="jetbrains.mps.lang.smodel.structure.OperationParm_Inclusion" flags="ng" index="1xIGOp" />
      <concept id="1144101972840" name="jetbrains.mps.lang.smodel.structure.OperationParm_Concept" flags="ng" index="1xMEDy">
        <child id="1207343664468" name="conceptArgument" index="ri$Ld" />
      </concept>
      <concept id="1180636770613" name="jetbrains.mps.lang.smodel.structure.SNodeCreator" flags="nn" index="3zrR0B">
        <child id="1180636770616" name="createdType" index="3zrR0E" />
      </concept>
      <concept id="1144146199828" name="jetbrains.mps.lang.smodel.structure.Node_CopyOperation" flags="nn" index="1$rogu" />
      <concept id="1139867745658" name="jetbrains.mps.lang.smodel.structure.Node_ReplaceWithNewOperation" flags="nn" index="1_qnLN">
        <reference id="1139867957129" name="concept" index="1_rbq0" />
      </concept>
      <concept id="1144195091934" name="jetbrains.mps.lang.smodel.structure.Node_IsRoleOperation" flags="nn" index="1BlSNk">
        <reference id="1144195362400" name="conceptOfParent" index="1BmUXE" />
        <reference id="1144195396777" name="linkInParent" index="1Bn3mz" />
      </concept>
      <concept id="1140131837776" name="jetbrains.mps.lang.smodel.structure.Node_ReplaceWithAnotherOperation" flags="nn" index="1P9Npp">
        <child id="1140131861877" name="replacementNode" index="1P9ThW" />
      </concept>
      <concept id="1140137987495" name="jetbrains.mps.lang.smodel.structure.SNodeTypeCastExpression" flags="nn" index="1PxgMI" />
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="c7fb639f-be78-4307-89b0-b5959c3fa8c8" name="jetbrains.mps.lang.text">
      <concept id="155656958578482948" name="jetbrains.mps.lang.text.structure.Word" flags="ng" index="3oM_SD">
        <property id="155656958578482949" name="value" index="3oM_SC" />
      </concept>
      <concept id="2535923850359271782" name="jetbrains.mps.lang.text.structure.Line" flags="ng" index="1PaTwC">
        <child id="2535923850359271783" name="elements" index="1PaTwD" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1204796164442" name="jetbrains.mps.baseLanguage.collections.structure.InternalSequenceOperation" flags="nn" index="23sCx2">
        <child id="1204796294226" name="closure" index="23t8la" />
      </concept>
      <concept id="1176906603202" name="jetbrains.mps.baseLanguage.collections.structure.BinaryOperation" flags="nn" index="56pJg">
        <child id="1176906787974" name="rightExpression" index="576Qk" />
      </concept>
      <concept id="540871147943773365" name="jetbrains.mps.baseLanguage.collections.structure.SingleArgumentSequenceOperation" flags="nn" index="25WWJ4">
        <child id="540871147943773366" name="argument" index="25WWJ7" />
      </concept>
      <concept id="1237467461002" name="jetbrains.mps.baseLanguage.collections.structure.GetIteratorOperation" flags="nn" index="uNJiE" />
      <concept id="1151688443754" name="jetbrains.mps.baseLanguage.collections.structure.ListType" flags="in" index="_YKpA">
        <child id="1151688676805" name="elementType" index="_ZDj9" />
      </concept>
      <concept id="1151689724996" name="jetbrains.mps.baseLanguage.collections.structure.SequenceType" flags="in" index="A3Dl8">
        <child id="1151689745422" name="elementType" index="A3Ik2" />
      </concept>
      <concept id="1151702311717" name="jetbrains.mps.baseLanguage.collections.structure.ToListOperation" flags="nn" index="ANE8D" />
      <concept id="1237721394592" name="jetbrains.mps.baseLanguage.collections.structure.AbstractContainerCreator" flags="nn" index="HWqM0">
        <child id="1237721435808" name="initValue" index="HW$Y0" />
        <child id="1237721435807" name="elementType" index="HW$YZ" />
      </concept>
      <concept id="1203518072036" name="jetbrains.mps.baseLanguage.collections.structure.SmartClosureParameterDeclaration" flags="ig" index="Rh6nW" />
      <concept id="1160600644654" name="jetbrains.mps.baseLanguage.collections.structure.ListCreatorWithInit" flags="nn" index="Tc6Ow" />
      <concept id="1160666733551" name="jetbrains.mps.baseLanguage.collections.structure.AddAllElementsOperation" flags="nn" index="X8dFx" />
      <concept id="1201792049884" name="jetbrains.mps.baseLanguage.collections.structure.TranslateOperation" flags="nn" index="3goQfb" />
      <concept id="1165525191778" name="jetbrains.mps.baseLanguage.collections.structure.GetFirstOperation" flags="nn" index="1uHKPH" />
      <concept id="1165595910856" name="jetbrains.mps.baseLanguage.collections.structure.GetLastOperation" flags="nn" index="1yVyf7" />
      <concept id="1202120902084" name="jetbrains.mps.baseLanguage.collections.structure.WhereOperation" flags="nn" index="3zZkjj" />
      <concept id="1202128969694" name="jetbrains.mps.baseLanguage.collections.structure.SelectOperation" flags="nn" index="3$u5V9" />
      <concept id="1176501494711" name="jetbrains.mps.baseLanguage.collections.structure.IsNotEmptyOperation" flags="nn" index="3GX2aA" />
      <concept id="1180964022718" name="jetbrains.mps.baseLanguage.collections.structure.ConcatOperation" flags="nn" index="3QWeyG" />
    </language>
  </registry>
  <node concept="24kQdi" id="4vv0wgO7CaK">
    <ref role="1XX52x" to="c8s6:4vv0wgO7Ca6" resolve="Grammar" />
    <node concept="3EZMnI" id="4vv0wgO7CaM" role="2wV5jI">
      <node concept="l2Vlx" id="4vv0wgO7CaN" role="2iSdaV" />
      <node concept="3F0ifn" id="4vv0wgO7CaO" role="3EZMnx">
        <property role="3F0ifm" value="grammar" />
      </node>
      <node concept="3F0A7n" id="4vv0wgO7CaY" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="4vv0wgO7Cb7" role="3EZMnx">
        <property role="3F0ifm" value="{" />
      </node>
      <node concept="3EZMnI" id="4vv0wgO8fvj" role="3EZMnx">
        <node concept="VPM3Z" id="4vv0wgO8fvl" role="3F10Kt" />
        <node concept="3F0ifn" id="4vv0wgO8fwp" role="3EZMnx">
          <property role="3F0ifm" value="terminals:" />
        </node>
        <node concept="l2Vlx" id="4vv0wgO8fvo" role="2iSdaV" />
        <node concept="3F2HdR" id="4vv0wgO7CbK" role="3EZMnx">
          <ref role="1NtTu8" to="c8s6:4vv0wgO7Cbt" resolve="terminals" />
          <node concept="l2Vlx" id="4vv0wgO7CbM" role="2czzBx" />
          <node concept="lj46D" id="4vv0wgO7Ccy" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="pj6Ft" id="4vv0wgO7Cc_" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="pVoyu" id="4vv0wgO7Ccw" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="pVoyu" id="4vv0wgO8fwi" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="4vv0wgO8fwl" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3EZMnI" id="4vv0wgO8fx1" role="3EZMnx">
        <node concept="VPM3Z" id="4vv0wgO8fx3" role="3F10Kt" />
        <node concept="3F0ifn" id="4vv0wgO8fxC" role="3EZMnx">
          <property role="3F0ifm" value="nonterminals:" />
        </node>
        <node concept="l2Vlx" id="4vv0wgO8fx6" role="2iSdaV" />
        <node concept="pVoyu" id="4vv0wgO8fyq" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="4vv0wgO8fx_" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="3F2HdR" id="4vv0wgO7Cc1" role="3EZMnx">
          <ref role="1NtTu8" to="c8s6:4vv0wgO7Cbv" resolve="nonterminals" />
          <node concept="l2Vlx" id="4vv0wgO7Cc3" role="2czzBx" />
          <node concept="lj46D" id="4vv0wgO7CcD" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="pj6Ft" id="4vv0wgO7CcF" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="pVoyu" id="4vv0wgO7Cds" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
      </node>
      <node concept="3EZMnI" id="4vv0wgO8fz2" role="3EZMnx">
        <node concept="VPM3Z" id="4vv0wgO8fz4" role="3F10Kt" />
        <node concept="3F0ifn" id="4vv0wgO8fzJ" role="3EZMnx">
          <property role="3F0ifm" value="rules:" />
        </node>
        <node concept="3F2HdR" id="4vv0wgOau8a" role="3EZMnx">
          <ref role="1NtTu8" to="c8s6:4vv0wgO7CcX" resolve="rules" />
          <node concept="2EHx9g" id="4vv0wgOau8m" role="2czzBx" />
          <node concept="pVoyu" id="4vv0wgOau8h" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="lj46D" id="4vv0wgOau8j" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="l2Vlx" id="4vv0wgO8fz7" role="2iSdaV" />
        <node concept="pVoyu" id="4vv0wgO8f$s" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="4vv0wgO8fzG" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3EZMnI" id="4vv0wgOalpo" role="3EZMnx">
        <node concept="VPM3Z" id="4vv0wgOalpp" role="3F10Kt" />
        <node concept="3F0ifn" id="4vv0wgOalpq" role="3EZMnx">
          <property role="3F0ifm" value="derived:" />
        </node>
        <node concept="3F2HdR" id="4vv0wgOau8t" role="3EZMnx">
          <ref role="1NtTu8" to="c8s6:4vv0wgOalpj" resolve="derivedInfo" />
          <node concept="2iRkQZ" id="4vv0wgOau8w" role="2czzBx" />
          <node concept="VPM3Z" id="4vv0wgOau8x" role="3F10Kt" />
          <node concept="pVoyu" id="4vv0wgOau8A" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="lj46D" id="4vv0wgOau8D" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="l2Vlx" id="4vv0wgOalpr" role="2iSdaV" />
        <node concept="pVoyu" id="4vv0wgOalps" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="4vv0wgOalpt" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="4vv0wgO7Cbh" role="3EZMnx">
        <property role="3F0ifm" value="}" />
        <node concept="pVoyu" id="4vv0wgOa4X4" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="4vv0wgO7Cff">
    <ref role="1XX52x" to="c8s6:4vv0wgO7Ca7" resolve="GrammarRule" />
    <node concept="3EZMnI" id="4vv0wgO7Cfm" role="2wV5jI">
      <node concept="3EZMnI" id="4vv0wgO8Dpt" role="3EZMnx">
        <node concept="VPM3Z" id="4vv0wgO8Dpv" role="3F10Kt" />
        <node concept="3F0A7n" id="4vv0wgO8DoX" role="3EZMnx">
          <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
        </node>
        <node concept="3F0ifn" id="4vv0wgO8Dpb" role="3EZMnx">
          <property role="3F0ifm" value=":" />
          <node concept="11L4FC" id="4vv0wgO8Lk9" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="3$7jql" id="4vv0wgOfi6F" role="3F10Kt">
            <property role="3$6WeP" value="1" />
          </node>
        </node>
        <node concept="2iRfu4" id="4vv0wgO8Dpy" role="2iSdaV" />
      </node>
      <node concept="3EZMnI" id="4vv0wgOf$1a" role="3EZMnx">
        <node concept="VPM3Z" id="4vv0wgOf$1c" role="3F10Kt" />
        <node concept="l2Vlx" id="4vv0wgOf$1f" role="2iSdaV" />
        <node concept="3F1sOY" id="4vv0wgO7Cft" role="3EZMnx">
          <ref role="1NtTu8" to="c8s6:4vv0wgO7Cai" resolve="leftHandSide" />
        </node>
        <node concept="3F0ifn" id="4vv0wgO7Cfz" role="3EZMnx">
          <property role="3F0ifm" value="::=" />
        </node>
        <node concept="3F2HdR" id="4vv0wgO7CfF" role="3EZMnx">
          <property role="2czwfO" value=" " />
          <ref role="1NtTu8" to="c8s6:4vv0wgO7Cak" resolve="rightHandSide" />
          <node concept="l2Vlx" id="4vv0wgO7CfH" role="2czzBx" />
        </node>
      </node>
      <node concept="2iRfu4" id="4vv0wgO8Dqd" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="4vv0wgO7M0j">
    <ref role="1XX52x" to="c8s6:4vv0wgO7Cac" resolve="TerminalReference" />
    <node concept="1iCGBv" id="4vv0wgO7M0l" role="2wV5jI">
      <ref role="1NtTu8" to="c8s6:4vv0wgO7Cad" resolve="declaration" />
      <node concept="1sVBvm" id="4vv0wgO7M0n" role="1sWHZn">
        <node concept="3F0A7n" id="4vv0wgO7M0u" role="2wV5jI">
          <property role="1Intyy" value="true" />
          <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="4vv0wgO7M0U">
    <ref role="1XX52x" to="c8s6:4vv0wgO7Caf" resolve="NonterminalReference" />
    <node concept="1iCGBv" id="4vv0wgO7M0W" role="2wV5jI">
      <ref role="1NtTu8" to="c8s6:4vv0wgO7Cag" resolve="declaration" />
      <node concept="1sVBvm" id="4vv0wgO7M0Y" role="1sWHZn">
        <node concept="3F0A7n" id="4vv0wgO7M15" role="2wV5jI">
          <property role="1Intyy" value="true" />
          <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="4vv0wgO7Rn$">
    <ref role="1XX52x" to="c8s6:4vv0wgO7VEI" resolve="SymbolDeclaration" />
    <node concept="3F0A7n" id="4vv0wgO7RnA" role="2wV5jI">
      <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      <node concept="VPRnO" id="4vv0wgO82Ks" role="3F10Kt">
        <property role="VOm3f" value="true" />
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="4vv0wgO7VFa">
    <ref role="aqKnT" to="c8s6:4vv0wgO7Ca8" resolve="TerminalDeclaration" />
    <node concept="3eGOop" id="4vv0wgO7VFh" role="3ft7WO">
      <node concept="ucgPf" id="4vv0wgO7VFj" role="3aKz83">
        <node concept="3clFbS" id="4vv0wgO7VFl" role="2VODD2">
          <node concept="3clFbF" id="4vv0wgO7WvF" role="3cqZAp">
            <node concept="2pJPEk" id="4vv0wgO7WvD" role="3clFbG">
              <node concept="2pJPED" id="4vv0wgO7Wzp" role="2pJPEn">
                <ref role="2pJxaS" to="c8s6:4vv0wgO7Ca8" resolve="TerminalDeclaration" />
                <node concept="2pJxcG" id="4vv0wgO7WCr" role="2pJxcM">
                  <ref role="2pJxcJ" to="tpck:h0TrG11" resolve="name" />
                  <node concept="ub8z3" id="4vv0wgO7WF4" role="28ntcv" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL3D" id="4vv0wgO7VHE" role="upBLP">
        <node concept="16Na2f" id="4vv0wgO7VHF" role="16NL3A">
          <node concept="3clFbS" id="4vv0wgO7VHG" role="2VODD2">
            <node concept="3clFbF" id="4vv0wgO7VLY" role="3cqZAp">
              <node concept="2OqwBi" id="4vv0wgO7W8r" role="3clFbG">
                <node concept="ub8z3" id="4vv0wgO7VLX" role="2Oq$k0" />
                <node concept="17RvpY" id="4vv0wgO7WuX" role="2OqNvi" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="4vv0wgO86O1">
    <ref role="aqKnT" to="c8s6:4vv0wgO7Ca9" resolve="NonterminalDeclaration" />
    <node concept="3eGOop" id="4vv0wgO86O2" role="3ft7WO">
      <node concept="ucgPf" id="4vv0wgO86O3" role="3aKz83">
        <node concept="3clFbS" id="4vv0wgO86O4" role="2VODD2">
          <node concept="3clFbF" id="4vv0wgO86O5" role="3cqZAp">
            <node concept="2pJPEk" id="4vv0wgO86O6" role="3clFbG">
              <node concept="2pJPED" id="4vv0wgO86O7" role="2pJPEn">
                <ref role="2pJxaS" to="c8s6:4vv0wgO7Ca9" resolve="NonterminalDeclaration" />
                <node concept="2pJxcG" id="4vv0wgO86O8" role="2pJxcM">
                  <ref role="2pJxcJ" to="tpck:h0TrG11" resolve="name" />
                  <node concept="ub8z3" id="4vv0wgO86O9" role="28ntcv" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL3D" id="4vv0wgO86Oa" role="upBLP">
        <node concept="16Na2f" id="4vv0wgO86Ob" role="16NL3A">
          <node concept="3clFbS" id="4vv0wgO86Oc" role="2VODD2">
            <node concept="3clFbF" id="4vv0wgO86Od" role="3cqZAp">
              <node concept="2OqwBi" id="4vv0wgO86Oe" role="3clFbG">
                <node concept="ub8z3" id="4vv0wgO86Of" role="2Oq$k0" />
                <node concept="17RvpY" id="4vv0wgO86Og" role="2OqNvi" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="4vv0wgO8kkT">
    <property role="3GE5qa" value="sample" />
    <ref role="1XX52x" to="c8s6:4vv0wgO8jGv" resolve="GrammarExamples" />
    <node concept="3EZMnI" id="4vv0wgO8klC" role="2wV5jI">
      <node concept="l2Vlx" id="4vv0wgO8klD" role="2iSdaV" />
      <node concept="3F0ifn" id="4vv0wgO8klE" role="3EZMnx">
        <property role="3F0ifm" value="examples" />
      </node>
      <node concept="3F0A7n" id="4vv0wgO8kn9" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="4vv0wgO8knT" role="3EZMnx">
        <property role="3F0ifm" value="for" />
      </node>
      <node concept="1iCGBv" id="4vv0wgO8klG" role="3EZMnx">
        <ref role="1NtTu8" to="c8s6:4vv0wgO8klz" resolve="grammar" />
        <node concept="1sVBvm" id="4vv0wgO8klJ" role="1sWHZn">
          <node concept="3F0A7n" id="4vv0wgO8klL" role="2wV5jI">
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="4vv0wgO8klM" role="3EZMnx">
        <property role="3F0ifm" value="{" />
        <node concept="3mYdg7" id="4vv0wgO8klN" role="3F10Kt">
          <property role="1413C4" value="body-brace" />
        </node>
        <node concept="ljvvj" id="4vv0wgO8klO" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="4vv0wgO8klW" role="3EZMnx">
        <ref role="1NtTu8" to="c8s6:4vv0wgO8kku" resolve="lines" />
        <node concept="l2Vlx" id="4vv0wgO8klX" role="2czzBx" />
        <node concept="pj6Ft" id="4vv0wgO8klY" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="4vv0wgO8klZ" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="ljvvj" id="4vv0wgO8km0" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="4vv0wgO8km1" role="3EZMnx">
        <property role="3F0ifm" value="}" />
        <node concept="3mYdg7" id="4vv0wgO8km2" role="3F10Kt">
          <property role="1413C4" value="body-brace" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="4vv0wgO8koZ">
    <property role="3GE5qa" value="sample" />
    <ref role="1XX52x" to="c8s6:4vv0wgO8jGw" resolve="ExampleLine" />
    <node concept="3EZMnI" id="4vv0wgO8kp1" role="2wV5jI">
      <node concept="2yq9I_" id="4vv0wgO9JCA" role="3EZMnx">
        <ref role="225u1j" to="c8s6:4vv0wgO9JCv" resolve="expand" />
        <node concept="jv8YD" id="4vv0wgO9JCI" role="1563LE" />
      </node>
      <node concept="l2Vlx" id="4vv0wgO8kp2" role="2iSdaV" />
      <node concept="3F1sOY" id="4vv0wgO8kpb" role="3EZMnx">
        <ref role="1NtTu8" to="c8s6:4vv0wgO8jGx" resolve="leftHandSide" />
      </node>
      <node concept="3F0ifn" id="4vv0wgO8kpc" role="3EZMnx">
        <property role="3F0ifm" value="-&gt;" />
      </node>
      <node concept="1QoScp" id="4vv0wgO9JD_" role="3EZMnx">
        <property role="1QpmdY" value="true" />
        <node concept="pkWqt" id="4vv0wgO9JDC" role="3e4ffs">
          <node concept="3clFbS" id="4vv0wgO9JDE" role="2VODD2">
            <node concept="3clFbF" id="4vv0wgO9JHX" role="3cqZAp">
              <node concept="2OqwBi" id="4vv0wgO9JUF" role="3clFbG">
                <node concept="pncrf" id="4vv0wgO9JHW" role="2Oq$k0" />
                <node concept="3TrcHB" id="4vv0wgO9K7c" role="2OqNvi">
                  <ref role="3TsBF5" to="c8s6:4vv0wgO9JCv" resolve="expand" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3F1sOY" id="4vv0wgO8kph" role="1QoS34">
          <ref role="1NtTu8" to="c8s6:4vv0wgO8jGz" resolve="rightHandSide" />
          <node concept="2w$q5c" id="4vv0wgO9JDl" role="3xwHhi">
            <node concept="2aJ2om" id="4vv0wgO9JDp" role="2w$qW5">
              <ref role="2$4xQ3" node="4vv0wgO9JCL" resolve="expand" />
            </node>
          </node>
        </node>
        <node concept="3F1sOY" id="4vv0wgO9JHL" role="1QoVPY">
          <ref role="1NtTu8" to="c8s6:4vv0wgO8jGz" resolve="rightHandSide" />
          <node concept="2w$q5c" id="4vv0wgO9JHM" role="3xwHhi" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="4vv0wgO8rm2">
    <property role="3GE5qa" value="sample" />
    <ref role="1XX52x" to="c8s6:4vv0wgO8jGA" resolve="TerminalInstance" />
    <node concept="1iCGBv" id="4vv0wgO8rm4" role="2wV5jI">
      <ref role="1NtTu8" to="c8s6:4vv0wgO8jGB" resolve="symbol" />
      <node concept="1sVBvm" id="4vv0wgO8rm6" role="1sWHZn">
        <node concept="3F0A7n" id="4vv0wgO8rmd" role="2wV5jI">
          <property role="1Intyy" value="true" />
          <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
        </node>
      </node>
      <node concept="A1WHr" id="3v$U6ONOPWq" role="3vIgyS">
        <ref role="2ZyFGn" to="c8s6:4vv0wgO8jGA" resolve="TerminalInstance" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="4vv0wgO8SN_">
    <property role="3GE5qa" value="sample" />
    <ref role="1XX52x" to="c8s6:4vv0wgO8jGD" resolve="NonterminalInstance" />
    <node concept="3EZMnI" id="4vv0wgO9tof" role="2wV5jI">
      <node concept="1iCGBv" id="4vv0wgO9A0w" role="3EZMnx">
        <ref role="1NtTu8" to="c8s6:4vv0wgO8jGE" resolve="rule" />
        <node concept="1sVBvm" id="4vv0wgO9A0y" role="1sWHZn">
          <node concept="3F0A7n" id="4vv0wgO9A0G" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="4vv0wgO9A0R" role="3EZMnx">
        <property role="3F0ifm" value="{" />
        <node concept="ljvvj" id="4vv0wgO9A16" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="4vv0wgO8SOh" role="3EZMnx">
        <property role="2czwfO" value="," />
        <ref role="1NtTu8" to="c8s6:4vv0wgO8jGN" resolve="children" />
        <node concept="l2Vlx" id="4vv0wgO8SOj" role="2czzBx" />
        <node concept="lj46D" id="4vv0wgO9A12" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pj6Ft" id="4vv0wgO9A1a" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="4vv0wgO9toz" role="3EZMnx">
        <property role="3F0ifm" value="}" />
        <node concept="pVoyu" id="4vv0wgO9A18" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="l2Vlx" id="4vv0wgO9toD" role="2iSdaV" />
    </node>
    <node concept="2aJ2om" id="4vv0wgO9JCN" role="CpUAK">
      <ref role="2$4xQ3" node="4vv0wgO9JCL" resolve="expand" />
    </node>
  </node>
  <node concept="2ABfQD" id="4vv0wgO9JCu">
    <property role="3GE5qa" value="sample" />
    <property role="TrG5h" value="expandCollapse" />
    <node concept="2BsEeg" id="4vv0wgO9JCL" role="2ABdcP">
      <property role="2gpH_U" value="true" />
      <property role="TrG5h" value="expand" />
    </node>
  </node>
  <node concept="24kQdi" id="4vv0wgO9JCP">
    <property role="3GE5qa" value="sample" />
    <ref role="1XX52x" to="c8s6:4vv0wgO8jGD" resolve="NonterminalInstance" />
    <node concept="3EZMnI" id="4vv0wgO9JCQ" role="6VMZX">
      <node concept="2iRfu4" id="4vv0wgO9JCR" role="2iSdaV" />
      <node concept="3F0ifn" id="4vv0wgO9JCS" role="3EZMnx">
        <property role="3F0ifm" value="rule:" />
      </node>
      <node concept="1iCGBv" id="4vv0wgO9JCT" role="3EZMnx">
        <ref role="1NtTu8" to="c8s6:4vv0wgO8jGE" resolve="rule" />
        <node concept="1sVBvm" id="4vv0wgO9JCU" role="1sWHZn">
          <node concept="3F0A7n" id="4vv0wgO9JCV" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
    </node>
    <node concept="3F2HdR" id="4vv0wgO9JD2" role="2wV5jI">
      <ref role="1NtTu8" to="c8s6:4vv0wgO8jGN" resolve="children" />
      <node concept="l2Vlx" id="4vv0wgO9JD3" role="2czzBx" />
    </node>
  </node>
  <node concept="24kQdi" id="4vv0wgOacLz">
    <property role="3GE5qa" value="LL1" />
    <ref role="1XX52x" to="c8s6:4vv0wgOacKS" resolve="LL1Row" />
    <node concept="3EZMnI" id="4vv0wgOacL_" role="2wV5jI">
      <node concept="3F1sOY" id="4vv0wgOacLK" role="3EZMnx">
        <ref role="1NtTu8" to="c8s6:4vv0wgOacKT" resolve="symbol" />
      </node>
      <node concept="3F2HdR" id="4vv0wgOacLQ" role="3EZMnx">
        <ref role="1NtTu8" to="c8s6:4vv0wgOacKV" resolve="firstSymbol" />
        <node concept="2iRfu4" id="4vv0wgOaQJB" role="2czzBx" />
        <node concept="VPXOz" id="4vv0wgOaIxY" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="2iRfu4" id="4vv0wgOacLC" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="4vv0wgOacMC">
    <property role="3GE5qa" value="LL1" />
    <ref role="1XX52x" to="c8s6:4vv0wgOacKR" resolve="LL1Table" />
    <node concept="3EZMnI" id="4vv0wgOaYZs" role="2wV5jI">
      <node concept="3EZMnI" id="7DEtAv3SzmE" role="3EZMnx">
        <node concept="VPM3Z" id="7DEtAv3SzmG" role="3F10Kt" />
        <node concept="3F0ifn" id="7DEtAv3SzmI" role="3EZMnx">
          <property role="3F0ifm" value="First(_) table" />
        </node>
        <node concept="3F0A7n" id="7DEtAv3Szmp" role="3EZMnx">
          <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
        </node>
        <node concept="l2Vlx" id="7DEtAv3SzmJ" role="2iSdaV" />
      </node>
      <node concept="3F2HdR" id="4vv0wgOacMQ" role="3EZMnx">
        <ref role="1NtTu8" to="c8s6:4vv0wgOacL8" resolve="rows" />
        <node concept="2EHx9g" id="4vv0wgOacMV" role="2czzBx" />
        <node concept="VPXOz" id="4vv0wgOaAlp" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="2EHx9g" id="4vv0wgOaZ05" role="2iSdaV" />
    </node>
  </node>
  <node concept="3p36aQ" id="4vv0wgOfOoR">
    <property role="3GE5qa" value="LL1" />
    <ref role="aqKnT" to="c8s6:4vv0wgOacKR" resolve="LL1Table" />
    <node concept="3eGOop" id="4vv0wgOfOoS" role="3ft7WO">
      <node concept="ucgPf" id="4vv0wgOfOoT" role="3aKz83">
        <node concept="3clFbS" id="4vv0wgOfOoU" role="2VODD2">
          <node concept="3cpWs8" id="7DEtAv3JALX" role="3cqZAp">
            <node concept="3cpWsn" id="7DEtAv3JALY" role="3cpWs9">
              <property role="TrG5h" value="grammar" />
              <node concept="3Tqbb2" id="7DEtAv3JA_x" role="1tU5fm">
                <ref role="ehGHo" to="c8s6:4vv0wgO7Ca6" resolve="Grammar" />
              </node>
              <node concept="2OqwBi" id="7DEtAv3JALZ" role="33vP2m">
                <node concept="3bvxqY" id="7DEtAv3JAM0" role="2Oq$k0" />
                <node concept="2Xjw5R" id="7DEtAv3JAM1" role="2OqNvi">
                  <node concept="1xIGOp" id="7DEtAv3JAM2" role="1xVPHs" />
                  <node concept="1xMEDy" id="7DEtAv3JAM3" role="1xVPHs">
                    <node concept="chp4Y" id="7DEtAv3JAM4" role="ri$Ld">
                      <ref role="cht4Q" to="c8s6:4vv0wgO7Ca6" resolve="Grammar" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3cpWs8" id="7DEtAv3S$Lj" role="3cqZAp">
            <node concept="3cpWsn" id="7DEtAv3S$Lk" role="3cpWs9">
              <property role="TrG5h" value="result" />
              <node concept="3Tqbb2" id="7DEtAv3S$GE" role="1tU5fm">
                <ref role="ehGHo" to="c8s6:4vv0wgOacKR" resolve="LL1Table" />
              </node>
              <node concept="2OqwBi" id="7DEtAv3S$Ll" role="33vP2m">
                <node concept="35c_gC" id="7DEtAv3S$Lm" role="2Oq$k0">
                  <ref role="35c_gD" to="c8s6:4vv0wgOacKR" resolve="LL1Table" />
                </node>
                <node concept="2qgKlT" id="7DEtAv3S$Ln" role="2OqNvi">
                  <ref role="37wK5l" to="h9ya:4vv0wgOb$jc" resolve="computeTable" />
                  <node concept="2OqwBi" id="7DEtAv3S$Lo" role="37wK5m">
                    <node concept="37vLTw" id="7DEtAv3S$Lp" role="2Oq$k0">
                      <ref role="3cqZAo" node="7DEtAv3JALY" resolve="grammar" />
                    </node>
                    <node concept="3Tsc0h" id="7DEtAv3S$Lq" role="2OqNvi">
                      <ref role="3TtcxE" to="c8s6:4vv0wgO7Cbv" resolve="nonterminals" />
                    </node>
                  </node>
                  <node concept="2OqwBi" id="7DEtAv3S$Lr" role="37wK5m">
                    <node concept="37vLTw" id="7DEtAv3S$Ls" role="2Oq$k0">
                      <ref role="3cqZAo" node="7DEtAv3JALY" resolve="grammar" />
                    </node>
                    <node concept="3Tsc0h" id="7DEtAv3S$Lt" role="2OqNvi">
                      <ref role="3TtcxE" to="c8s6:4vv0wgO7CcX" resolve="rules" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="4vv0wgOhVTY" role="3cqZAp">
            <node concept="37vLTI" id="7DEtAv3S$zG" role="3clFbG">
              <node concept="Xl_RD" id="7DEtAv3S$Ec" role="37vLTx">
                <property role="Xl_RC" value="base grammar" />
              </node>
              <node concept="2OqwBi" id="7DEtAv3SzQl" role="37vLTJ">
                <node concept="37vLTw" id="7DEtAv3S$Lu" role="2Oq$k0">
                  <ref role="3cqZAo" node="7DEtAv3S$Lk" resolve="result" />
                </node>
                <node concept="3TrcHB" id="7DEtAv3S$4V" role="2OqNvi">
                  <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3cpWs6" id="7DEtAv3S$Rm" role="3cqZAp">
            <node concept="37vLTw" id="7DEtAv3S_6$" role="3cqZAk">
              <ref role="3cqZAo" node="7DEtAv3S$Lk" resolve="result" />
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL3D" id="4vv0wgOfY2W" role="upBLP">
        <node concept="16Na2f" id="4vv0wgOfY2X" role="16NL3A">
          <node concept="3clFbS" id="4vv0wgOfY2Y" role="2VODD2">
            <node concept="3clFbF" id="4vv0wgOfY7D" role="3cqZAp">
              <node concept="2OqwBi" id="4vv0wgOfYHs" role="3clFbG">
                <node concept="2OqwBi" id="4vv0wgOfYj_" role="2Oq$k0">
                  <node concept="3bvxqY" id="4vv0wgOfY7C" role="2Oq$k0" />
                  <node concept="2Xjw5R" id="4vv0wgOfYqV" role="2OqNvi">
                    <node concept="1xIGOp" id="4vv0wgOfZ5C" role="1xVPHs" />
                    <node concept="1xMEDy" id="4vv0wgOfYqX" role="1xVPHs">
                      <node concept="chp4Y" id="4vv0wgOfYyq" role="ri$Ld">
                        <ref role="cht4Q" to="c8s6:4vv0wgO7Ca6" resolve="Grammar" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3x8VRR" id="4vv0wgOfYWS" role="2OqNvi" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="7DEtAv3Jeot" role="upBLP">
        <node concept="2h3Zct" id="7DEtAv3Jepu" role="16NeZM">
          <property role="2h4Kg1" value="base grammar" />
        </node>
      </node>
      <node concept="16NL0t" id="7DEtAv3RHvJ" role="upBLP">
        <node concept="2h3Zct" id="7DEtAv3RHxh" role="16NL0q">
          <property role="2h4Kg1" value="first symbols table" />
        </node>
      </node>
    </node>
    <node concept="3eGOop" id="7DEtAv3Jepx" role="3ft7WO">
      <node concept="ucgPf" id="7DEtAv3Jepy" role="3aKz83">
        <node concept="3clFbS" id="7DEtAv3Jepz" role="2VODD2">
          <node concept="3cpWs8" id="7DEtAv3JCc$" role="3cqZAp">
            <node concept="3cpWsn" id="7DEtAv3JCc_" role="3cpWs9">
              <property role="TrG5h" value="grammar" />
              <node concept="3Tqbb2" id="7DEtAv3JCcA" role="1tU5fm">
                <ref role="ehGHo" to="c8s6:4vv0wgO7Ca6" resolve="Grammar" />
              </node>
              <node concept="2OqwBi" id="7DEtAv3JCcB" role="33vP2m">
                <node concept="3bvxqY" id="7DEtAv3JCcC" role="2Oq$k0" />
                <node concept="2Xjw5R" id="7DEtAv3JCcD" role="2OqNvi">
                  <node concept="1xIGOp" id="7DEtAv3JCcE" role="1xVPHs" />
                  <node concept="1xMEDy" id="7DEtAv3JCcF" role="1xVPHs">
                    <node concept="chp4Y" id="7DEtAv3JCcG" role="ri$Ld">
                      <ref role="cht4Q" to="c8s6:4vv0wgO7Ca6" resolve="Grammar" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3cpWs8" id="7DEtAv3S_iM" role="3cqZAp">
            <node concept="3cpWsn" id="7DEtAv3S_iN" role="3cpWs9">
              <property role="TrG5h" value="result" />
              <node concept="3Tqbb2" id="7DEtAv3Szxn" role="1tU5fm">
                <ref role="ehGHo" to="c8s6:4vv0wgOacKR" resolve="LL1Table" />
              </node>
              <node concept="2OqwBi" id="7DEtAv3S_iO" role="33vP2m">
                <node concept="35c_gC" id="7DEtAv3S_iP" role="2Oq$k0">
                  <ref role="35c_gD" to="c8s6:4vv0wgOacKR" resolve="LL1Table" />
                </node>
                <node concept="2qgKlT" id="7DEtAv3S_iQ" role="2OqNvi">
                  <ref role="37wK5l" to="h9ya:4vv0wgOb$jc" resolve="computeTable" />
                  <node concept="2OqwBi" id="7DEtAv3S_iR" role="37wK5m">
                    <node concept="37vLTw" id="7DEtAv3S_iS" role="2Oq$k0">
                      <ref role="3cqZAo" node="7DEtAv3JCc_" resolve="grammar" />
                    </node>
                    <node concept="3Tsc0h" id="7DEtAv3S_iT" role="2OqNvi">
                      <ref role="3TtcxE" to="c8s6:4vv0wgO7Cbv" resolve="nonterminals" />
                    </node>
                  </node>
                  <node concept="2OqwBi" id="7DEtAv3S_iU" role="37wK5m">
                    <node concept="2OqwBi" id="7DEtAv3S_iV" role="2Oq$k0">
                      <node concept="37vLTw" id="7DEtAv3S_iW" role="2Oq$k0">
                        <ref role="3cqZAo" node="7DEtAv3JCc_" resolve="grammar" />
                      </node>
                      <node concept="3Tsc0h" id="7DEtAv3S_iX" role="2OqNvi">
                        <ref role="3TtcxE" to="c8s6:4vv0wgO7CcX" resolve="rules" />
                      </node>
                    </node>
                    <node concept="3QWeyG" id="7DEtAv3S_iY" role="2OqNvi">
                      <node concept="2OqwBi" id="7DEtAv3S_iZ" role="576Qk">
                        <node concept="2OqwBi" id="7DEtAv3S_j0" role="2Oq$k0">
                          <node concept="2OqwBi" id="7DEtAv3S_j1" role="2Oq$k0">
                            <node concept="37vLTw" id="7DEtAv3S_j2" role="2Oq$k0">
                              <ref role="3cqZAo" node="7DEtAv3JCc_" resolve="grammar" />
                            </node>
                            <node concept="3Tsc0h" id="7DEtAv3S_j3" role="2OqNvi">
                              <ref role="3TtcxE" to="c8s6:4vv0wgOalpj" resolve="derivedInfo" />
                            </node>
                          </node>
                          <node concept="v3k3i" id="7DEtAv3S_j4" role="2OqNvi">
                            <node concept="chp4Y" id="7DEtAv3S_j5" role="v3oSu">
                              <ref role="cht4Q" to="c8s6:3v$U6ONNksH" resolve="AdditionalRulesTable" />
                            </node>
                          </node>
                        </node>
                        <node concept="13MTOL" id="7DEtAv3S_j6" role="2OqNvi">
                          <ref role="13MTZf" to="c8s6:3v$U6ONNksI" resolve="rules" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="7DEtAv3S_sL" role="3cqZAp">
            <node concept="37vLTI" id="7DEtAv3S_sM" role="3clFbG">
              <node concept="Xl_RD" id="7DEtAv3S_sN" role="37vLTx">
                <property role="Xl_RC" value="augmented grammar" />
              </node>
              <node concept="2OqwBi" id="7DEtAv3S_sO" role="37vLTJ">
                <node concept="37vLTw" id="7DEtAv3S_sP" role="2Oq$k0">
                  <ref role="3cqZAo" node="7DEtAv3S_iN" resolve="result" />
                </node>
                <node concept="3TrcHB" id="7DEtAv3S_sQ" role="2OqNvi">
                  <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3cpWs6" id="7DEtAv3S_sR" role="3cqZAp">
            <node concept="37vLTw" id="7DEtAv3S_sS" role="3cqZAk">
              <ref role="3cqZAo" node="7DEtAv3S_iN" resolve="result" />
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL3D" id="7DEtAv3JepI" role="upBLP">
        <node concept="16Na2f" id="7DEtAv3JepJ" role="16NL3A">
          <node concept="3clFbS" id="7DEtAv3JepK" role="2VODD2">
            <node concept="3clFbF" id="7DEtAv3JepL" role="3cqZAp">
              <node concept="2OqwBi" id="7DEtAv3JepM" role="3clFbG">
                <node concept="2OqwBi" id="7DEtAv3JepN" role="2Oq$k0">
                  <node concept="3bvxqY" id="7DEtAv3JepO" role="2Oq$k0" />
                  <node concept="2Xjw5R" id="7DEtAv3JepP" role="2OqNvi">
                    <node concept="1xIGOp" id="7DEtAv3JepQ" role="1xVPHs" />
                    <node concept="1xMEDy" id="7DEtAv3JepR" role="1xVPHs">
                      <node concept="chp4Y" id="7DEtAv3JepS" role="ri$Ld">
                        <ref role="cht4Q" to="c8s6:4vv0wgO7Ca6" resolve="Grammar" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3x8VRR" id="7DEtAv3JepT" role="2OqNvi" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="7DEtAv3JepU" role="upBLP">
        <node concept="2h3Zct" id="7DEtAv3JepV" role="16NeZM">
          <property role="2h4Kg1" value="augmented grammar" />
        </node>
      </node>
      <node concept="16NL0t" id="7DEtAv3RHxk" role="upBLP">
        <node concept="2h3Zct" id="7DEtAv3RHxl" role="16NL0q">
          <property role="2h4Kg1" value="first symbols table" />
        </node>
      </node>
    </node>
    <node concept="3eGOop" id="7DEtAv3T1dZ" role="3ft7WO">
      <node concept="ucgPf" id="7DEtAv3T1e0" role="3aKz83">
        <node concept="3clFbS" id="7DEtAv3T1e1" role="2VODD2">
          <node concept="3cpWs8" id="7DEtAv3T1eb" role="3cqZAp">
            <node concept="3cpWsn" id="7DEtAv3T1ec" role="3cpWs9">
              <property role="TrG5h" value="result" />
              <node concept="3Tqbb2" id="7DEtAv3T1ed" role="1tU5fm">
                <ref role="ehGHo" to="c8s6:4vv0wgOacKR" resolve="LL1Table" />
              </node>
              <node concept="2OqwBi" id="7DEtAv3T3z_" role="33vP2m">
                <node concept="2OqwBi" id="7DEtAv41MqD" role="2Oq$k0">
                  <node concept="2OqwBi" id="7DEtAv41KBR" role="2Oq$k0">
                    <node concept="2OqwBi" id="7DEtAv3T2Lh" role="2Oq$k0">
                      <node concept="1yR$tW" id="7DEtAv3T2Li" role="2Oq$k0" />
                      <node concept="2Ttrtt" id="7DEtAv41Jv2" role="2OqNvi" />
                    </node>
                    <node concept="v3k3i" id="7DEtAv41LL7" role="2OqNvi">
                      <node concept="chp4Y" id="7DEtAv41LOC" role="v3oSu">
                        <ref role="cht4Q" to="c8s6:4vv0wgOacKR" resolve="LL1Table" />
                      </node>
                    </node>
                  </node>
                  <node concept="1uHKPH" id="7DEtAv41MJ6" role="2OqNvi" />
                </node>
                <node concept="1$rogu" id="7DEtAv3T3QC" role="2OqNvi" />
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="7DEtAv3TCVx" role="3cqZAp">
            <node concept="2OqwBi" id="7DEtAv3TDrL" role="3clFbG">
              <node concept="37vLTw" id="7DEtAv41YZh" role="2Oq$k0">
                <ref role="3cqZAo" node="7DEtAv3T1ec" resolve="result" />
              </node>
              <node concept="2qgKlT" id="7DEtAv3Wou9" role="2OqNvi">
                <ref role="37wK5l" to="h9ya:7DEtAv3TzeX" resolve="reduceTable" />
                <node concept="2OqwBi" id="7DEtAv3WsUn" role="37wK5m">
                  <node concept="2OqwBi" id="7DEtAv3WqKj" role="2Oq$k0">
                    <node concept="2OqwBi" id="7DEtAv40MKy" role="2Oq$k0">
                      <node concept="1yR$tW" id="7DEtAv40Mri" role="2Oq$k0" />
                      <node concept="2Ttrtt" id="7DEtAv40NjX" role="2OqNvi" />
                    </node>
                    <node concept="v3k3i" id="7DEtAv3WsiU" role="2OqNvi">
                      <node concept="chp4Y" id="7DEtAv3WsCi" role="v3oSu">
                        <ref role="cht4Q" to="c8s6:3v$U6ONMnQB" resolve="TransformationsTable" />
                      </node>
                    </node>
                  </node>
                  <node concept="13MTOL" id="7DEtAv3WtqW" role="2OqNvi">
                    <ref role="13MTZf" to="c8s6:3v$U6ONMnQE" resolve="transformations" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="7DEtAv3T1ex" role="3cqZAp">
            <node concept="37vLTI" id="7DEtAv3T1ey" role="3clFbG">
              <node concept="2OqwBi" id="7DEtAv3T1e$" role="37vLTJ">
                <node concept="37vLTw" id="7DEtAv3T1e_" role="2Oq$k0">
                  <ref role="3cqZAo" node="7DEtAv3T1ec" resolve="result" />
                </node>
                <node concept="3TrcHB" id="7DEtAv3T1eA" role="2OqNvi">
                  <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                </node>
              </node>
              <node concept="3cpWs3" id="7DEtAv3T4lH" role="37vLTx">
                <node concept="2OqwBi" id="7DEtAv3T4Gt" role="3uHU7w">
                  <node concept="37vLTw" id="7DEtAv3T4rs" role="2Oq$k0">
                    <ref role="3cqZAo" node="7DEtAv3T1ec" resolve="result" />
                  </node>
                  <node concept="3TrcHB" id="7DEtAv3T50S" role="2OqNvi">
                    <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                  </node>
                </node>
                <node concept="Xl_RD" id="7DEtAv3T3Xt" role="3uHU7B">
                  <property role="Xl_RC" value="reduced: " />
                </node>
              </node>
            </node>
          </node>
          <node concept="3cpWs6" id="7DEtAv3T1eB" role="3cqZAp">
            <node concept="37vLTw" id="7DEtAv3T1eC" role="3cqZAk">
              <ref role="3cqZAo" node="7DEtAv3T1ec" resolve="result" />
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL3D" id="7DEtAv3T1eD" role="upBLP">
        <node concept="16Na2f" id="7DEtAv3T1eE" role="16NL3A">
          <node concept="3clFbS" id="7DEtAv3T1eF" role="2VODD2">
            <node concept="3clFbF" id="7DEtAv3T1Kz" role="3cqZAp">
              <node concept="2OqwBi" id="7DEtAv41IFk" role="3clFbG">
                <node concept="2OqwBi" id="7DEtAv3T2k2" role="2Oq$k0">
                  <node concept="2OqwBi" id="7DEtAv3T1YP" role="2Oq$k0">
                    <node concept="1yR$tW" id="7DEtAv3T1Kp" role="2Oq$k0" />
                    <node concept="2Ttrtt" id="7DEtAv41GNQ" role="2OqNvi" />
                  </node>
                  <node concept="v3k3i" id="7DEtAv41Ik$" role="2OqNvi">
                    <node concept="chp4Y" id="7DEtAv41Im1" role="v3oSu">
                      <ref role="cht4Q" to="c8s6:4vv0wgOacKR" resolve="LL1Table" />
                    </node>
                  </node>
                </node>
                <node concept="3GX2aA" id="7DEtAv41IUX" role="2OqNvi" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="7DEtAv3T1eP" role="upBLP">
        <node concept="2h3Zct" id="7DEtAv3T1eQ" role="16NeZM">
          <property role="2h4Kg1" value="reduced grammar" />
        </node>
      </node>
      <node concept="16NL0t" id="7DEtAv3T1eR" role="upBLP">
        <node concept="2h3Zct" id="7DEtAv3T1eS" role="16NL0q">
          <property role="2h4Kg1" value="first symbols table" />
        </node>
      </node>
    </node>
    <node concept="3VyMlK" id="4vv0wgOfZa0" role="3ft7WO" />
  </node>
  <node concept="24kQdi" id="4vv0wgOgpik">
    <property role="3GE5qa" value="LR0" />
    <ref role="1XX52x" to="c8s6:4vv0wgOgoDN" resolve="RuleCursor" />
    <node concept="3F0ifn" id="4vv0wgOgpim" role="2wV5jI">
      <property role="3F0ifm" value="|" />
    </node>
  </node>
  <node concept="24kQdi" id="4vv0wgOhkuR">
    <property role="3GE5qa" value="LR0" />
    <ref role="1XX52x" to="c8s6:4vv0wgOgoDJ" resolve="LR0Table" />
    <node concept="3EZMnI" id="4vv0wgOhkuT" role="2wV5jI">
      <node concept="3F0ifn" id="4vv0wgOhkv0" role="3EZMnx">
        <property role="3F0ifm" value="LR(0) items:" />
      </node>
      <node concept="3F2HdR" id="4vv0wgOhkv6" role="3EZMnx">
        <ref role="1NtTu8" to="c8s6:4vv0wgOgoDL" resolve="items" />
        <node concept="2iRkQZ" id="4vv0wgOhkv8" role="2czzBx" />
      </node>
      <node concept="2iRkQZ" id="4vv0wgOhkuW" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="4vv0wgOpZFE">
    <property role="3GE5qa" value="LL1" />
    <ref role="1XX52x" to="c8s6:4vv0wgOnpJi" resolve="SymbolWithRules" />
    <node concept="3EZMnI" id="4vv0wgOpZFG" role="2wV5jI">
      <node concept="l2Vlx" id="4vv0wgOpZFH" role="2iSdaV" />
      <node concept="1iCGBv" id="7DEtAv3Qk6d" role="3EZMnx">
        <ref role="1NtTu8" to="c8s6:7DEtAv3PZGf" resolve="symbol" />
        <node concept="1sVBvm" id="7DEtAv3Qk6f" role="1sWHZn">
          <node concept="3F0A7n" id="7DEtAv3Qk6r" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="3F2HdR" id="4vv0wgOpZFV" role="3EZMnx">
        <ref role="1NtTu8" to="c8s6:4vv0wgOnpJj" resolve="paths" />
        <node concept="l2Vlx" id="4vv0wgOpZFW" role="2czzBx" />
        <node concept="pj6Ft" id="4vv0wgOpZFX" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="4vv0wgOpZFY" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="4vv0wgOpZHl" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="VPXOz" id="4vv0wgOqhWo" role="3F10Kt">
        <property role="VOm3f" value="true" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="4vv0wgOpZHM">
    <property role="3GE5qa" value="LL1" />
    <ref role="1XX52x" to="c8s6:4vv0wgOnpJc" resolve="RulePath" />
    <node concept="3F2HdR" id="4vv0wgOpZIg" role="2wV5jI">
      <property role="2czwfO" value="," />
      <ref role="1NtTu8" to="c8s6:4vv0wgOnpJg" resolve="rules" />
      <node concept="l2Vlx" id="4vv0wgOpZIh" role="2czzBx" />
    </node>
  </node>
  <node concept="24kQdi" id="4vv0wgOq8tR">
    <property role="3GE5qa" value="LL1" />
    <ref role="1XX52x" to="c8s6:4vv0wgOnpJd" resolve="RuleReference" />
    <node concept="1iCGBv" id="4vv0wgOq8tX" role="2wV5jI">
      <ref role="1NtTu8" to="c8s6:4vv0wgOnpJe" resolve="declaration" />
      <node concept="1sVBvm" id="4vv0wgOq8u0" role="1sWHZn">
        <node concept="3F0A7n" id="4vv0wgOq8u2" role="2wV5jI">
          <property role="1Intyy" value="true" />
          <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="3v$U6ONMnPw">
    <property role="3GE5qa" value="transform" />
    <ref role="1XX52x" to="c8s6:3v$U6ONMnP1" resolve="Transform" />
    <node concept="3EZMnI" id="3v$U6ONMnPy" role="2wV5jI">
      <node concept="1HlG4h" id="4fqD4t1QqD4" role="3EZMnx">
        <node concept="VechU" id="4fqD4t1Rw8V" role="3F10Kt">
          <property role="Vb096" value="fLJRk5_/gray" />
        </node>
        <node concept="1HfYo3" id="4fqD4t1QqD6" role="1HlULh">
          <node concept="3TQlhw" id="4fqD4t1QqD8" role="1Hhtcw">
            <node concept="3clFbS" id="4fqD4t1QqDa" role="2VODD2">
              <node concept="3clFbF" id="4fqD4t1QqMv" role="3cqZAp">
                <node concept="3cpWs3" id="4fqD4t1QshH" role="3clFbG">
                  <node concept="Xl_RD" id="4fqD4t1Qsit" role="3uHU7w">
                    <property role="Xl_RC" value=":" />
                  </node>
                  <node concept="2OqwBi" id="4fqD4t1QqYb" role="3uHU7B">
                    <node concept="pncrf" id="4fqD4t1QqMu" role="2Oq$k0" />
                    <node concept="2bSWHS" id="4fqD4t1Qr7p" role="2OqNvi" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="3v$U6ONMnPD" role="3EZMnx">
        <property role="3F0ifm" value="transform" />
      </node>
      <node concept="3F0A7n" id="7DEtAv3lMVu" role="3EZMnx">
        <ref role="1NtTu8" to="c8s6:7DEtAv3lMVa" resolve="side" />
      </node>
      <node concept="3F0ifn" id="7DEtAv3z9UV" role="3EZMnx">
        <property role="3F0ifm" value="for" />
      </node>
      <node concept="3F1sOY" id="7DEtAv3z9Vh" role="3EZMnx">
        <ref role="1NtTu8" to="c8s6:7DEtAv3yrWD" resolve="symbol" />
      </node>
      <node concept="3F0ifn" id="7DEtAv3oMaM" role="3EZMnx">
        <property role="3F0ifm" value="from" />
      </node>
      <node concept="3F1sOY" id="7DEtAv3oMbC" role="3EZMnx">
        <ref role="1NtTu8" to="c8s6:7DEtAv3oLdb" resolve="fromPattern" />
      </node>
      <node concept="3F0ifn" id="3v$U6ONMnPR" role="3EZMnx">
        <property role="3F0ifm" value="to" />
      </node>
      <node concept="3F1sOY" id="7DEtAv3ysUp" role="3EZMnx">
        <ref role="1NtTu8" to="c8s6:7DEtAv3ysTw" resolve="toPattern" />
      </node>
      <node concept="l2Vlx" id="3v$U6ONMnP_" role="2iSdaV" />
      <node concept="3EZMnI" id="7DEtAv3$TPT" role="3EZMnx">
        <node concept="VPM3Z" id="7DEtAv3$TPV" role="3F10Kt" />
        <node concept="1HlG4h" id="7DEtAv3$TQj" role="3EZMnx">
          <node concept="1HfYo3" id="7DEtAv3$TQl" role="1HlULh">
            <node concept="3TQlhw" id="7DEtAv3$TQn" role="1Hhtcw">
              <node concept="3clFbS" id="7DEtAv3$TQp" role="2VODD2">
                <node concept="3clFbF" id="7DEtAv3$TV2" role="3cqZAp">
                  <node concept="2OqwBi" id="7DEtAv3$X8L" role="3clFbG">
                    <node concept="2OqwBi" id="7DEtAv3$Url" role="2Oq$k0">
                      <node concept="2OqwBi" id="7DEtAv3$U6I" role="2Oq$k0">
                        <node concept="pncrf" id="7DEtAv3$TV1" role="2Oq$k0" />
                        <node concept="3TrEf2" id="7DEtAv3$UfK" role="2OqNvi">
                          <ref role="3Tt5mk" to="c8s6:7DEtAv3oLdb" resolve="fromPattern" />
                        </node>
                      </node>
                      <node concept="2qgKlT" id="7DEtAv3$UA2" role="2OqNvi">
                        <ref role="37wK5l" to="h9ya:7DEtAv3qu1D" resolve="getConsequence" />
                        <node concept="2OqwBi" id="7DEtAv3$V8y" role="37wK5m">
                          <node concept="pncrf" id="7DEtAv3$UYi" role="2Oq$k0" />
                          <node concept="2qgKlT" id="7DEtAv3$Vxr" role="2OqNvi">
                            <ref role="37wK5l" to="h9ya:7DEtAv3xC4_" resolve="getSymbol" />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="liA8E" id="7DEtAv3$YZq" role="2OqNvi">
                      <ref role="37wK5l" to="wyt6:~Object.toString()" resolve="toString" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="VechU" id="7DEtAv3_osS" role="3F10Kt">
            <property role="Vb096" value="fLJRk5_/gray" />
          </node>
        </node>
        <node concept="3F0ifn" id="7DEtAv3$Z9i" role="3EZMnx">
          <property role="3F0ifm" value="=&gt;" />
          <node concept="VechU" id="7DEtAv3_ovj" role="3F10Kt">
            <property role="Vb096" value="fLJRk5_/gray" />
          </node>
        </node>
        <node concept="1HlG4h" id="7DEtAv3$Zg3" role="3EZMnx">
          <node concept="VechU" id="7DEtAv3_ovo" role="3F10Kt">
            <property role="Vb096" value="fLJRk5_/gray" />
          </node>
          <node concept="1HfYo3" id="7DEtAv3$Zg4" role="1HlULh">
            <node concept="3TQlhw" id="7DEtAv3$Zg5" role="1Hhtcw">
              <node concept="3clFbS" id="7DEtAv3$Zg6" role="2VODD2">
                <node concept="3clFbF" id="7DEtAv3$Zg7" role="3cqZAp">
                  <node concept="2OqwBi" id="7DEtAv3$Zg8" role="3clFbG">
                    <node concept="2OqwBi" id="7DEtAv3$Zg9" role="2Oq$k0">
                      <node concept="2OqwBi" id="7DEtAv3$Zga" role="2Oq$k0">
                        <node concept="pncrf" id="7DEtAv3$Zgb" role="2Oq$k0" />
                        <node concept="3TrEf2" id="7DEtAv3$ZBt" role="2OqNvi">
                          <ref role="3Tt5mk" to="c8s6:7DEtAv3ysTw" resolve="toPattern" />
                        </node>
                      </node>
                      <node concept="2qgKlT" id="7DEtAv3$Zgd" role="2OqNvi">
                        <ref role="37wK5l" to="h9ya:7DEtAv3qu1D" resolve="getConsequence" />
                        <node concept="2OqwBi" id="7DEtAv3$Zge" role="37wK5m">
                          <node concept="pncrf" id="7DEtAv3$Zgf" role="2Oq$k0" />
                          <node concept="2qgKlT" id="7DEtAv3$Zgg" role="2OqNvi">
                            <ref role="37wK5l" to="h9ya:7DEtAv3xC4_" resolve="getSymbol" />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="liA8E" id="7DEtAv3$Zgh" role="2OqNvi">
                      <ref role="37wK5l" to="wyt6:~Object.toString()" resolve="toString" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="l2Vlx" id="7DEtAv3$TPY" role="2iSdaV" />
        <node concept="pVoyu" id="7DEtAv3$TQc" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="7DEtAv3$TQf" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="3v$U6ONMnR5">
    <property role="3GE5qa" value="transform" />
    <ref role="1XX52x" to="c8s6:3v$U6ONMnQB" resolve="TransformationsTable" />
    <node concept="3EZMnI" id="3v$U6ONMnR7" role="2wV5jI">
      <node concept="3F0ifn" id="3v$U6ONMnRe" role="3EZMnx">
        <property role="3F0ifm" value="transformations" />
      </node>
      <node concept="3F0A7n" id="7DEtAv409Pc" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="7DEtAv409Pu" role="3EZMnx">
        <property role="3F0ifm" value=":" />
        <node concept="11L4FC" id="7DEtAv41dkN" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="3v$U6ONMnRn" role="3EZMnx">
        <ref role="1NtTu8" to="c8s6:3v$U6ONMnQE" resolve="transformations" />
        <node concept="l2Vlx" id="3v$U6ONMnRt" role="2czzBx" />
        <node concept="pVoyu" id="3v$U6ONMnRz" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="3v$U6ONMnR_" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pj6Ft" id="3v$U6ONMnRC" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="l2Vlx" id="3v$U6ONMnRw" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="3v$U6ONNljj">
    <property role="3GE5qa" value="transform" />
    <ref role="1XX52x" to="c8s6:3v$U6ONNksH" resolve="AdditionalRulesTable" />
    <node concept="3EZMnI" id="3v$U6ONNljq" role="2wV5jI">
      <node concept="3F0ifn" id="3v$U6ONNljx" role="3EZMnx">
        <property role="3F0ifm" value="additional rules:" />
      </node>
      <node concept="3F2HdR" id="3v$U6ONNljB" role="3EZMnx">
        <ref role="1NtTu8" to="c8s6:3v$U6ONNksI" resolve="rules" />
        <node concept="l2Vlx" id="3v$U6ONNljD" role="2czzBx" />
        <node concept="pVoyu" id="3v$U6ONNljH" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="3v$U6ONNljJ" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pj6Ft" id="3v$U6ONNljM" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="l2Vlx" id="3v$U6ONNljt" role="2iSdaV" />
    </node>
  </node>
  <node concept="IW6AY" id="3v$U6ONOPFD">
    <property role="3GE5qa" value="sample" />
    <ref role="aqKnT" to="c8s6:4vv0wgO8jGA" resolve="TerminalInstance" />
    <node concept="1Qtc8_" id="3v$U6ONOPFE" role="IW6Ez">
      <node concept="1GhOrh" id="3v$U6ONP5nU" role="1Qtc8A">
        <node concept="1GhMSn" id="3v$U6ONP5nW" role="1GhOrs">
          <node concept="3clFbS" id="3v$U6ONP5nY" role="2VODD2">
            <node concept="3cpWs8" id="7DEtAv3ww0r" role="3cqZAp">
              <node concept="3cpWsn" id="7DEtAv3ww0s" role="3cpWs9">
                <property role="TrG5h" value="ancestors" />
                <node concept="A3Dl8" id="7DEtAv3wDsp" role="1tU5fm">
                  <node concept="3Tqbb2" id="7DEtAv3wEy_" role="A3Ik2" />
                </node>
                <node concept="2OqwBi" id="7DEtAv3ww0t" role="33vP2m">
                  <node concept="7Obwk" id="7DEtAv3ww0u" role="2Oq$k0" />
                  <node concept="z$bX8" id="7DEtAv3ww0v" role="2OqNvi">
                    <node concept="1xIGOp" id="7DEtAv3ww0w" role="1xVPHs" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3cpWs8" id="7DEtAv3tgzG" role="3cqZAp">
              <node concept="3cpWsn" id="7DEtAv3tgzH" role="3cpWs9">
                <property role="TrG5h" value="rightAncestors" />
                <node concept="_YKpA" id="7DEtAv3tgwZ" role="1tU5fm">
                  <node concept="3Tqbb2" id="7DEtAv3tgx2" role="_ZDj9">
                    <ref role="ehGHo" to="c8s6:4vv0wgO8jGD" resolve="NonterminalInstance" />
                  </node>
                </node>
                <node concept="2OqwBi" id="7DEtAv3w4_s" role="33vP2m">
                  <node concept="2OqwBi" id="7DEtAv3vZhd" role="2Oq$k0">
                    <node concept="2YIFZM" id="7DEtAv3w_83" role="2Oq$k0">
                      <ref role="37wK5l" to="h9ya:7DEtAv3wvXr" resolve="takeWhile" />
                      <ref role="1Pybhc" to="h9ya:7DEtAv3wvTd" resolve="SequenceUtil" />
                      <node concept="37vLTw" id="7DEtAv3w_q2" role="37wK5m">
                        <ref role="3cqZAo" node="7DEtAv3ww0s" resolve="ancestors" />
                      </node>
                      <node concept="1bVj0M" id="7DEtAv3vOwz" role="37wK5m">
                        <node concept="37vLTG" id="7DEtAv3wAR1" role="1bW2Oz">
                          <property role="TrG5h" value="it" />
                          <node concept="3Tqbb2" id="7DEtAv3wD8g" role="1tU5fm" />
                        </node>
                        <node concept="3clFbS" id="7DEtAv3vOw$" role="1bW5cS">
                          <node concept="3clFbF" id="7DEtAv3vOw_" role="3cqZAp">
                            <node concept="1Wc70l" id="7DEtAv3vOwA" role="3clFbG">
                              <node concept="2OqwBi" id="7DEtAv3vOwB" role="3uHU7w">
                                <node concept="2OqwBi" id="7DEtAv3vOwC" role="2Oq$k0">
                                  <node concept="37vLTw" id="7DEtAv3wCO7" role="2Oq$k0">
                                    <ref role="3cqZAo" node="7DEtAv3wAR1" resolve="it" />
                                  </node>
                                  <node concept="YCak7" id="7DEtAv3vOwE" role="2OqNvi" />
                                </node>
                                <node concept="3w_OXm" id="7DEtAv3vOwF" role="2OqNvi" />
                              </node>
                              <node concept="2OqwBi" id="7DEtAv3vOwG" role="3uHU7B">
                                <node concept="37vLTw" id="7DEtAv3wBY4" role="2Oq$k0">
                                  <ref role="3cqZAo" node="7DEtAv3wAR1" resolve="it" />
                                </node>
                                <node concept="1BlSNk" id="7DEtAv3vOwI" role="2OqNvi">
                                  <ref role="1BmUXE" to="c8s6:4vv0wgO8jGD" resolve="NonterminalInstance" />
                                  <ref role="1Bn3mz" to="c8s6:4vv0wgO8jGN" resolve="children" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="3$u5V9" id="7DEtAv3vZWy" role="2OqNvi">
                      <node concept="1bVj0M" id="7DEtAv3vZW$" role="23t8la">
                        <node concept="3clFbS" id="7DEtAv3vZW_" role="1bW5cS">
                          <node concept="3clFbF" id="7DEtAv3w0k1" role="3cqZAp">
                            <node concept="1PxgMI" id="7DEtAv3w1Dc" role="3clFbG">
                              <node concept="chp4Y" id="7DEtAv3w1Yn" role="3oSUPX">
                                <ref role="cht4Q" to="c8s6:4vv0wgO8jGD" resolve="NonterminalInstance" />
                              </node>
                              <node concept="2OqwBi" id="7DEtAv3w3_M" role="1m5AlR">
                                <node concept="37vLTw" id="7DEtAv3w0k0" role="2Oq$k0">
                                  <ref role="3cqZAo" node="7DEtAv3vZWA" resolve="it" />
                                </node>
                                <node concept="1mfA1w" id="7DEtAv3w42K" role="2OqNvi" />
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="Rh6nW" id="7DEtAv3vZWA" role="1bW2Oz">
                          <property role="TrG5h" value="it" />
                          <node concept="2jxLKc" id="7DEtAv3vZWB" role="1tU5fm" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="ANE8D" id="7DEtAv3w5cr" role="2OqNvi" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="3v$U6ONP5ue" role="3cqZAp">
              <node concept="2OqwBi" id="3v$U6ONP94n" role="3clFbG">
                <node concept="37vLTw" id="7DEtAv3tgzQ" role="2Oq$k0">
                  <ref role="3cqZAo" node="7DEtAv3tgzH" resolve="rightAncestors" />
                </node>
                <node concept="3goQfb" id="3v$U6ONPc_L" role="2OqNvi">
                  <node concept="1bVj0M" id="3v$U6ONPc_N" role="23t8la">
                    <node concept="3clFbS" id="3v$U6ONPc_O" role="1bW5cS">
                      <node concept="3clFbF" id="3v$U6ONPc_P" role="3cqZAp">
                        <node concept="2OqwBi" id="3v$U6ONPkXx" role="3clFbG">
                          <node concept="2OqwBi" id="3v$U6ONPha9" role="2Oq$k0">
                            <node concept="2OqwBi" id="7DEtAv40D3I" role="2Oq$k0">
                              <node concept="2OqwBi" id="3v$U6ONPfch" role="2Oq$k0">
                                <node concept="2OqwBi" id="3v$U6ONPeAj" role="2Oq$k0">
                                  <node concept="2OqwBi" id="3v$U6ONPdWK" role="2Oq$k0">
                                    <node concept="37vLTw" id="3v$U6ONPdNT" role="2Oq$k0">
                                      <ref role="3cqZAo" node="3v$U6ONPc_V" resolve="ancestor" />
                                    </node>
                                    <node concept="3TrEf2" id="3v$U6ONPeg$" role="2OqNvi">
                                      <ref role="3Tt5mk" to="c8s6:4vv0wgO8jGE" resolve="rule" />
                                    </node>
                                  </node>
                                  <node concept="2Rxl7S" id="3v$U6ONPeUv" role="2OqNvi" />
                                </node>
                                <node concept="2Rf3mk" id="3v$U6ONPfoJ" role="2OqNvi">
                                  <node concept="1xMEDy" id="3v$U6ONPfoL" role="1xVPHs">
                                    <node concept="chp4Y" id="7DEtAv40A$m" role="ri$Ld">
                                      <ref role="cht4Q" to="c8s6:3v$U6ONMnQB" resolve="TransformationsTable" />
                                    </node>
                                  </node>
                                </node>
                              </node>
                              <node concept="13MTOL" id="7DEtAv40FFd" role="2OqNvi">
                                <ref role="13MTZf" to="c8s6:3v$U6ONMnQE" resolve="transformations" />
                              </node>
                            </node>
                            <node concept="3zZkjj" id="3v$U6ONPiK6" role="2OqNvi">
                              <node concept="1bVj0M" id="3v$U6ONPiK8" role="23t8la">
                                <node concept="3clFbS" id="3v$U6ONPiK9" role="1bW5cS">
                                  <node concept="3clFbF" id="3v$U6ONPjeN" role="3cqZAp">
                                    <node concept="1Wc70l" id="7DEtAv3ooCi" role="3clFbG">
                                      <node concept="2OqwBi" id="7DEtAv3oqbs" role="3uHU7B">
                                        <node concept="2OqwBi" id="7DEtAv3opeV" role="2Oq$k0">
                                          <node concept="37vLTw" id="7DEtAv3ooQ7" role="2Oq$k0">
                                            <ref role="3cqZAo" node="3v$U6ONPiKa" resolve="transform" />
                                          </node>
                                          <node concept="3TrcHB" id="7DEtAv3opMk" role="2OqNvi">
                                            <ref role="3TsBF5" to="c8s6:7DEtAv3lMVa" resolve="side" />
                                          </node>
                                        </node>
                                        <node concept="21noJN" id="7DEtAv3oqx0" role="2OqNvi">
                                          <node concept="21nZrQ" id="7DEtAv3oqMm" role="21noJM">
                                            <ref role="21nZrZ" to="tpc2:3Ftr4R6BFex" resolve="RIGHT" />
                                          </node>
                                        </node>
                                      </node>
                                      <node concept="2OqwBi" id="7DEtAv3BlmG" role="3uHU7w">
                                        <node concept="2OqwBi" id="7DEtAv3nJ2Z" role="2Oq$k0">
                                          <node concept="2qgKlT" id="7DEtAv3nJmk" role="2OqNvi">
                                            <ref role="37wK5l" to="h9ya:7DEtAv3rlSg" resolve="matches" />
                                            <node concept="37vLTw" id="7DEtAv3Au6o" role="37wK5m">
                                              <ref role="3cqZAo" node="3v$U6ONPc_V" resolve="ancestor" />
                                            </node>
                                            <node concept="2OqwBi" id="7DEtAv3AjkO" role="37wK5m">
                                              <node concept="37vLTw" id="7DEtAv3AiYP" role="2Oq$k0">
                                                <ref role="3cqZAo" node="3v$U6ONPiKa" resolve="transform" />
                                              </node>
                                              <node concept="2qgKlT" id="7DEtAv3AjI3" role="2OqNvi">
                                                <ref role="37wK5l" to="h9ya:7DEtAv3xC4_" resolve="getSymbol" />
                                              </node>
                                            </node>
                                          </node>
                                          <node concept="2OqwBi" id="7DEtAv3Ag$L" role="2Oq$k0">
                                            <node concept="37vLTw" id="7DEtAv3nJEk" role="2Oq$k0">
                                              <ref role="3cqZAo" node="3v$U6ONPiKa" resolve="transform" />
                                            </node>
                                            <node concept="3TrEf2" id="7DEtAv3Ahkz" role="2OqNvi">
                                              <ref role="3Tt5mk" to="c8s6:7DEtAv3oLdb" resolve="fromPattern" />
                                            </node>
                                          </node>
                                        </node>
                                        <node concept="liA8E" id="7DEtAv3Bm0m" role="2OqNvi">
                                          <ref role="37wK5l" to="33ny:~Optional.isPresent()" resolve="isPresent" />
                                        </node>
                                      </node>
                                    </node>
                                  </node>
                                </node>
                                <node concept="Rh6nW" id="3v$U6ONPiKa" role="1bW2Oz">
                                  <property role="TrG5h" value="transform" />
                                  <node concept="2jxLKc" id="3v$U6ONPiKb" role="1tU5fm" />
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="3$u5V9" id="3v$U6ONPlic" role="2OqNvi">
                            <node concept="1bVj0M" id="3v$U6ONPlie" role="23t8la">
                              <node concept="3clFbS" id="3v$U6ONPlif" role="1bW5cS">
                                <node concept="3clFbF" id="3v$U6ONPlts" role="3cqZAp">
                                  <node concept="1Ls8ON" id="3v$U6ONPltr" role="3clFbG">
                                    <node concept="37vLTw" id="3v$U6ONPlQw" role="1Lso8e">
                                      <ref role="3cqZAo" node="3v$U6ONPc_V" resolve="ancestor" />
                                    </node>
                                    <node concept="37vLTw" id="3v$U6ONPmnJ" role="1Lso8e">
                                      <ref role="3cqZAo" node="3v$U6ONPlig" resolve="transform" />
                                    </node>
                                  </node>
                                </node>
                              </node>
                              <node concept="Rh6nW" id="3v$U6ONPlig" role="1bW2Oz">
                                <property role="TrG5h" value="transform" />
                                <node concept="2jxLKc" id="3v$U6ONPlih" role="1tU5fm" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="Rh6nW" id="3v$U6ONPc_V" role="1bW2Oz">
                      <property role="TrG5h" value="ancestor" />
                      <node concept="2jxLKc" id="3v$U6ONPc_W" role="1tU5fm" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="IWgqT" id="3v$U6ONPoAy" role="1GhOri">
          <node concept="1hCUdq" id="3v$U6ONPoA$" role="1hCUd6">
            <node concept="3clFbS" id="3v$U6ONPoAA" role="2VODD2">
              <node concept="3clFbF" id="7DEtAv3n7qb" role="3cqZAp">
                <node concept="2OqwBi" id="7DEtAv3ngkj" role="3clFbG">
                  <node concept="2OqwBi" id="7DEtAv3ncvd" role="2Oq$k0">
                    <node concept="2OqwBi" id="7DEtAv3zFtG" role="2Oq$k0">
                      <node concept="2OqwBi" id="7DEtAv3n88N" role="2Oq$k0">
                        <node concept="1LFfDK" id="7DEtAv3n7XT" role="2Oq$k0">
                          <node concept="3cmrfG" id="7DEtAv3n7YT" role="1LF_Uc">
                            <property role="3cmrfH" value="1" />
                          </node>
                          <node concept="2ZBlsa" id="7DEtAv3n7q9" role="1LFl5Q" />
                        </node>
                        <node concept="3TrEf2" id="7DEtAv3zFj8" role="2OqNvi">
                          <ref role="3Tt5mk" to="c8s6:7DEtAv3ysTw" resolve="toPattern" />
                        </node>
                      </node>
                      <node concept="2qgKlT" id="7DEtAv3zFIt" role="2OqNvi">
                        <ref role="37wK5l" to="h9ya:7DEtAv3qu1D" resolve="getConsequence" />
                        <node concept="2OqwBi" id="7DEtAv3zGMZ" role="37wK5m">
                          <node concept="1LFfDK" id="7DEtAv3zGzT" role="2Oq$k0">
                            <node concept="3cmrfG" id="7DEtAv3zGB6" role="1LF_Uc">
                              <property role="3cmrfH" value="1" />
                            </node>
                            <node concept="2ZBlsa" id="7DEtAv3zG86" role="1LFl5Q" />
                          </node>
                          <node concept="2qgKlT" id="7DEtAv3zHEy" role="2OqNvi">
                            <ref role="37wK5l" to="h9ya:7DEtAv3xC4_" resolve="getSymbol" />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="1yVyf7" id="7DEtAv3nea0" role="2OqNvi" />
                  </node>
                  <node concept="3TrcHB" id="7DEtAv3ng$X" role="2OqNvi">
                    <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="IWg2L" id="3v$U6ONPoAC" role="IWgqQ">
            <node concept="3clFbS" id="3v$U6ONPoAE" role="2VODD2">
              <node concept="3cpWs8" id="7DEtAv3Alto" role="3cqZAp">
                <node concept="3cpWsn" id="7DEtAv3Altp" role="3cpWs9">
                  <property role="TrG5h" value="nodeToApply" />
                  <node concept="3Tqbb2" id="7DEtAv3Alpl" role="1tU5fm">
                    <ref role="ehGHo" to="c8s6:4vv0wgO8jGD" resolve="NonterminalInstance" />
                  </node>
                  <node concept="1LFfDK" id="7DEtAv3Altq" role="33vP2m">
                    <node concept="3cmrfG" id="7DEtAv3Altr" role="1LF_Uc">
                      <property role="3cmrfH" value="0" />
                    </node>
                    <node concept="2ZBlsa" id="7DEtAv3Alts" role="1LFl5Q" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs8" id="7DEtAv3AlAu" role="3cqZAp">
                <node concept="3cpWsn" id="7DEtAv3AlAv" role="3cpWs9">
                  <property role="TrG5h" value="transform" />
                  <node concept="3Tqbb2" id="7DEtAv3AlAw" role="1tU5fm">
                    <ref role="ehGHo" to="c8s6:3v$U6ONMnP1" resolve="Transform" />
                  </node>
                  <node concept="1LFfDK" id="7DEtAv3AlAx" role="33vP2m">
                    <node concept="2ZBlsa" id="7DEtAv3AlAz" role="1LFl5Q" />
                    <node concept="3cmrfG" id="7DEtAv3AlGH" role="1LF_Uc">
                      <property role="3cmrfH" value="1" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3cpWs8" id="7DEtAv3BnwB" role="3cqZAp">
                <node concept="3cpWsn" id="7DEtAv3BnwC" role="3cpWs9">
                  <property role="TrG5h" value="matched" />
                  <node concept="_YKpA" id="7DEtAv3Bnsk" role="1tU5fm">
                    <node concept="3Tqbb2" id="7DEtAv3Bnsn" role="_ZDj9">
                      <ref role="ehGHo" to="c8s6:4vv0wgO8jGL" resolve="SymbolInstance" />
                    </node>
                  </node>
                  <node concept="2OqwBi" id="7DEtAv3BnwD" role="33vP2m">
                    <node concept="2OqwBi" id="7DEtAv3BnwE" role="2Oq$k0">
                      <node concept="2qgKlT" id="7DEtAv3BnwF" role="2OqNvi">
                        <ref role="37wK5l" to="h9ya:7DEtAv3rlSg" resolve="matches" />
                        <node concept="37vLTw" id="7DEtAv3BnwG" role="37wK5m">
                          <ref role="3cqZAo" node="7DEtAv3Altp" resolve="nodeToApply" />
                        </node>
                        <node concept="2OqwBi" id="7DEtAv3BnwH" role="37wK5m">
                          <node concept="37vLTw" id="7DEtAv3BnwI" role="2Oq$k0">
                            <ref role="3cqZAo" node="7DEtAv3AlAv" resolve="transform" />
                          </node>
                          <node concept="2qgKlT" id="7DEtAv3BnwJ" role="2OqNvi">
                            <ref role="37wK5l" to="h9ya:7DEtAv3xC4_" resolve="getSymbol" />
                          </node>
                        </node>
                      </node>
                      <node concept="2OqwBi" id="7DEtAv3BnwK" role="2Oq$k0">
                        <node concept="37vLTw" id="7DEtAv3BnwL" role="2Oq$k0">
                          <ref role="3cqZAo" node="7DEtAv3AlAv" resolve="transform" />
                        </node>
                        <node concept="3TrEf2" id="7DEtAv3BnwM" role="2OqNvi">
                          <ref role="3Tt5mk" to="c8s6:7DEtAv3oLdb" resolve="fromPattern" />
                        </node>
                      </node>
                    </node>
                    <node concept="liA8E" id="7DEtAv3BnwN" role="2OqNvi">
                      <ref role="37wK5l" to="33ny:~Optional.get()" resolve="get" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3cpWs8" id="7DEtAv3D$SJ" role="3cqZAp">
                <node concept="3cpWsn" id="7DEtAv3D$SK" role="3cpWs9">
                  <property role="TrG5h" value="symbolToAdd" />
                  <node concept="3Tqbb2" id="7DEtAv3D$GR" role="1tU5fm">
                    <ref role="ehGHo" to="c8s6:4vv0wgO8jGA" resolve="TerminalInstance" />
                  </node>
                  <node concept="2ShNRf" id="7DEtAv3DFrI" role="33vP2m">
                    <node concept="3zrR0B" id="7DEtAv3DFrG" role="2ShVmc">
                      <node concept="3Tqbb2" id="7DEtAv3DFrH" role="3zrR0E">
                        <ref role="ehGHo" to="c8s6:4vv0wgO8jGA" resolve="TerminalInstance" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="7DEtAv3DFFD" role="3cqZAp">
                <node concept="37vLTI" id="7DEtAv3DGxi" role="3clFbG">
                  <node concept="2OqwBi" id="7DEtAv3DFXv" role="37vLTJ">
                    <node concept="37vLTw" id="7DEtAv3DFFB" role="2Oq$k0">
                      <ref role="3cqZAo" node="7DEtAv3D$SK" resolve="symbolToAdd" />
                    </node>
                    <node concept="3TrEf2" id="7DEtAv3DJEG" role="2OqNvi">
                      <ref role="3Tt5mk" to="c8s6:4vv0wgO8jGB" resolve="symbol" />
                    </node>
                  </node>
                  <node concept="1PxgMI" id="7DEtAv3DDP1" role="37vLTx">
                    <node concept="chp4Y" id="7DEtAv3DEBH" role="3oSUPX">
                      <ref role="cht4Q" to="c8s6:4vv0wgO7Ca8" resolve="TerminalDeclaration" />
                    </node>
                    <node concept="2OqwBi" id="7DEtAv3D$SL" role="1m5AlR">
                      <node concept="2OqwBi" id="7DEtAv3D$SM" role="2Oq$k0">
                        <node concept="2OqwBi" id="7DEtAv3D$SN" role="2Oq$k0">
                          <node concept="1LFfDK" id="7DEtAv3D$SO" role="2Oq$k0">
                            <node concept="3cmrfG" id="7DEtAv3D$SP" role="1LF_Uc">
                              <property role="3cmrfH" value="1" />
                            </node>
                            <node concept="2ZBlsa" id="7DEtAv3D$SQ" role="1LFl5Q" />
                          </node>
                          <node concept="3TrEf2" id="7DEtAv3D$SR" role="2OqNvi">
                            <ref role="3Tt5mk" to="c8s6:7DEtAv3ysTw" resolve="toPattern" />
                          </node>
                        </node>
                        <node concept="2qgKlT" id="7DEtAv3D$SS" role="2OqNvi">
                          <ref role="37wK5l" to="h9ya:7DEtAv3qu1D" resolve="getConsequence" />
                          <node concept="2OqwBi" id="7DEtAv3D$ST" role="37wK5m">
                            <node concept="1LFfDK" id="7DEtAv3D$SU" role="2Oq$k0">
                              <node concept="3cmrfG" id="7DEtAv3D$SV" role="1LF_Uc">
                                <property role="3cmrfH" value="1" />
                              </node>
                              <node concept="2ZBlsa" id="7DEtAv3D$SW" role="1LFl5Q" />
                            </node>
                            <node concept="2qgKlT" id="7DEtAv3D$SX" role="2OqNvi">
                              <ref role="37wK5l" to="h9ya:7DEtAv3xC4_" resolve="getSymbol" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="1yVyf7" id="7DEtAv3D$SY" role="2OqNvi" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3cpWs8" id="7DEtAv3G3Ji" role="3cqZAp">
                <node concept="3cpWsn" id="7DEtAv3G3Jj" role="3cpWs9">
                  <property role="TrG5h" value="placeholder" />
                  <node concept="3Tqbb2" id="7DEtAv3G3G2" role="1tU5fm">
                    <ref role="ehGHo" to="c8s6:4vv0wgO8jGL" resolve="SymbolInstance" />
                  </node>
                  <node concept="2OqwBi" id="7DEtAv3G3Jk" role="33vP2m">
                    <node concept="37vLTw" id="7DEtAv3G3Jl" role="2Oq$k0">
                      <ref role="3cqZAo" node="7DEtAv3Altp" resolve="nodeToApply" />
                    </node>
                    <node concept="1_qnLN" id="7DEtAv3G3Jm" role="2OqNvi">
                      <ref role="1_rbq0" to="c8s6:4vv0wgO8jGD" resolve="NonterminalInstance" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="7DEtAv3G29N" role="3cqZAp">
                <node concept="2OqwBi" id="7DEtAv3G4SX" role="3clFbG">
                  <node concept="37vLTw" id="7DEtAv3G3Jn" role="2Oq$k0">
                    <ref role="3cqZAo" node="7DEtAv3G3Jj" resolve="placeholder" />
                  </node>
                  <node concept="1P9Npp" id="7DEtAv3G5K8" role="2OqNvi">
                    <node concept="2OqwBi" id="7DEtAv3Cz0B" role="1P9ThW">
                      <node concept="2OqwBi" id="7DEtAv3CyAr" role="2Oq$k0">
                        <node concept="37vLTw" id="7DEtAv3Cyqt" role="2Oq$k0">
                          <ref role="3cqZAo" node="7DEtAv3AlAv" resolve="transform" />
                        </node>
                        <node concept="3TrEf2" id="7DEtAv3CySV" role="2OqNvi">
                          <ref role="3Tt5mk" to="c8s6:7DEtAv3ysTw" resolve="toPattern" />
                        </node>
                      </node>
                      <node concept="2qgKlT" id="7DEtAv3CzdW" role="2OqNvi">
                        <ref role="37wK5l" to="h9ya:7DEtAv3BooQ" resolve="createInstance" />
                        <node concept="2OqwBi" id="7DEtAv3C$Os" role="37wK5m">
                          <node concept="2OqwBi" id="7DEtAv3DyJS" role="2Oq$k0">
                            <node concept="37vLTw" id="7DEtAv3Czei" role="2Oq$k0">
                              <ref role="3cqZAo" node="7DEtAv3BnwC" resolve="matched" />
                            </node>
                            <node concept="3QWeyG" id="7DEtAv3D$eD" role="2OqNvi">
                              <node concept="2ShNRf" id="7DEtAv3DGXg" role="576Qk">
                                <node concept="Tc6Ow" id="7DEtAv3DHbN" role="2ShVmc">
                                  <node concept="3Tqbb2" id="7DEtAv3DH$x" role="HW$YZ">
                                    <ref role="ehGHo" to="c8s6:4vv0wgO8jGL" resolve="SymbolInstance" />
                                  </node>
                                  <node concept="37vLTw" id="7DEtAv3DIRU" role="HW$Y0">
                                    <ref role="3cqZAo" node="7DEtAv3D$SK" resolve="symbolToAdd" />
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="uNJiE" id="7DEtAv3CAhC" role="2OqNvi" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3cqGtN" id="7DEtAv3n79C" role="2jZA2a">
            <node concept="3cqJkl" id="7DEtAv3n79D" role="3cqGtW">
              <node concept="3clFbS" id="7DEtAv3n79E" role="2VODD2">
                <node concept="3clFbF" id="3v$U6ONPoTD" role="3cqZAp">
                  <node concept="3cpWs3" id="3v$U6ONPsvJ" role="3clFbG">
                    <node concept="3cpWs3" id="3v$U6ONPsae" role="3uHU7B">
                      <node concept="3cpWs3" id="3v$U6ONPs0k" role="3uHU7B">
                        <node concept="3cpWs3" id="7DEtAv3uvAe" role="3uHU7B">
                          <node concept="2OqwBi" id="7DEtAv3uxje" role="3uHU7B">
                            <node concept="1LFfDK" id="7DEtAv3uwby" role="2Oq$k0">
                              <node concept="2ZBlsa" id="7DEtAv3uvDD" role="1LFl5Q" />
                              <node concept="3cmrfG" id="7DEtAv3uwFL" role="1LF_Uc">
                                <property role="3cmrfH" value="1" />
                              </node>
                            </node>
                            <node concept="2bSWHS" id="7DEtAv3uxOU" role="2OqNvi" />
                          </node>
                          <node concept="Xl_RD" id="3v$U6ONPrG8" role="3uHU7w">
                            <property role="Xl_RC" value=": from " />
                          </node>
                        </node>
                        <node concept="2OqwBi" id="3v$U6ONPqYM" role="3uHU7w">
                          <node concept="2OqwBi" id="3v$U6ONPqxt" role="2Oq$k0">
                            <node concept="1LFfDK" id="3v$U6ONPqh$" role="2Oq$k0">
                              <node concept="2ZBlsa" id="3v$U6ONPoTC" role="1LFl5Q" />
                              <node concept="3cmrfG" id="3v$U6ONPqz2" role="1LF_Uc">
                                <property role="3cmrfH" value="1" />
                              </node>
                            </node>
                            <node concept="3TrEf2" id="7DEtAv3slpY" role="2OqNvi">
                              <ref role="3Tt5mk" to="c8s6:7DEtAv3oLdb" resolve="fromPattern" />
                            </node>
                          </node>
                          <node concept="2qgKlT" id="7DEtAv3slL8" role="2OqNvi">
                            <ref role="37wK5l" to="h9ya:7DEtAv3qu1D" resolve="getConsequence" />
                            <node concept="2OqwBi" id="7DEtAv3snpY" role="37wK5m">
                              <node concept="1LFfDK" id="7DEtAv3smFF" role="2Oq$k0">
                                <node concept="3cmrfG" id="7DEtAv3smFP" role="1LF_Uc">
                                  <property role="3cmrfH" value="1" />
                                </node>
                                <node concept="2ZBlsa" id="7DEtAv3smbq" role="1LFl5Q" />
                              </node>
                              <node concept="2qgKlT" id="7DEtAv3xDxK" role="2OqNvi">
                                <ref role="37wK5l" to="h9ya:7DEtAv3xC4_" resolve="getSymbol" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="Xl_RD" id="3v$U6ONPs57" role="3uHU7w">
                        <property role="Xl_RC" value=" to " />
                      </node>
                    </node>
                    <node concept="2OqwBi" id="7DEtAv3zJS7" role="3uHU7w">
                      <node concept="2OqwBi" id="3v$U6ONPsxV" role="2Oq$k0">
                        <node concept="1LFfDK" id="3v$U6ONPsxW" role="2Oq$k0">
                          <node concept="2ZBlsa" id="3v$U6ONPsxX" role="1LFl5Q" />
                          <node concept="3cmrfG" id="3v$U6ONPsxY" role="1LF_Uc">
                            <property role="3cmrfH" value="1" />
                          </node>
                        </node>
                        <node concept="3TrEf2" id="7DEtAv3zJ4K" role="2OqNvi">
                          <ref role="3Tt5mk" to="c8s6:7DEtAv3ysTw" resolve="toPattern" />
                        </node>
                      </node>
                      <node concept="2qgKlT" id="7DEtAv3zK_d" role="2OqNvi">
                        <ref role="37wK5l" to="h9ya:7DEtAv3qu1D" resolve="getConsequence" />
                        <node concept="2OqwBi" id="7DEtAv3zLEX" role="37wK5m">
                          <node concept="1LFfDK" id="7DEtAv3zLsM" role="2Oq$k0">
                            <node concept="3cmrfG" id="7DEtAv3zLsW" role="1LF_Uc">
                              <property role="3cmrfH" value="1" />
                            </node>
                            <node concept="2ZBlsa" id="7DEtAv3zKZk" role="1LFl5Q" />
                          </node>
                          <node concept="2qgKlT" id="7DEtAv3zLQ1" role="2OqNvi">
                            <ref role="37wK5l" to="h9ya:7DEtAv3xC4_" resolve="getSymbol" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="27VH4U" id="7DEtAv3D_CA" role="2jiSrf">
            <node concept="3clFbS" id="7DEtAv3D_CB" role="2VODD2">
              <node concept="3SKdUt" id="7DEtAv3Gvsx" role="3cqZAp">
                <node concept="1PaTwC" id="7DEtAv3Gvsy" role="3ndbpf">
                  <node concept="3oM_SD" id="7DEtAv3GvL8" role="1PaTwD">
                    <property role="3oM_SC" value="todo:" />
                  </node>
                  <node concept="3oM_SD" id="7DEtAv3GvNv" role="1PaTwD">
                    <property role="3oM_SC" value="for" />
                  </node>
                  <node concept="3oM_SD" id="7DEtAv3GvNL" role="1PaTwD">
                    <property role="3oM_SC" value="nonterminals?" />
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="7DEtAv3DArJ" role="3cqZAp">
                <node concept="2OqwBi" id="7DEtAv3DCvg" role="3clFbG">
                  <node concept="2OqwBi" id="7DEtAv3DArL" role="2Oq$k0">
                    <node concept="2OqwBi" id="7DEtAv3DArM" role="2Oq$k0">
                      <node concept="2OqwBi" id="7DEtAv3DArN" role="2Oq$k0">
                        <node concept="1LFfDK" id="7DEtAv3DArO" role="2Oq$k0">
                          <node concept="3cmrfG" id="7DEtAv3DArP" role="1LF_Uc">
                            <property role="3cmrfH" value="1" />
                          </node>
                          <node concept="2ZBlsa" id="7DEtAv3DArQ" role="1LFl5Q" />
                        </node>
                        <node concept="3TrEf2" id="7DEtAv3DArR" role="2OqNvi">
                          <ref role="3Tt5mk" to="c8s6:7DEtAv3ysTw" resolve="toPattern" />
                        </node>
                      </node>
                      <node concept="2qgKlT" id="7DEtAv3DArS" role="2OqNvi">
                        <ref role="37wK5l" to="h9ya:7DEtAv3qu1D" resolve="getConsequence" />
                        <node concept="2OqwBi" id="7DEtAv3DArT" role="37wK5m">
                          <node concept="1LFfDK" id="7DEtAv3DArU" role="2Oq$k0">
                            <node concept="3cmrfG" id="7DEtAv3DArV" role="1LF_Uc">
                              <property role="3cmrfH" value="1" />
                            </node>
                            <node concept="2ZBlsa" id="7DEtAv3DArW" role="1LFl5Q" />
                          </node>
                          <node concept="2qgKlT" id="7DEtAv3DArX" role="2OqNvi">
                            <ref role="37wK5l" to="h9ya:7DEtAv3xC4_" resolve="getSymbol" />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="1yVyf7" id="7DEtAv3DArY" role="2OqNvi" />
                  </node>
                  <node concept="1mIQ4w" id="7DEtAv3DCWd" role="2OqNvi">
                    <node concept="chp4Y" id="7DEtAv3DD6I" role="cj9EA">
                      <ref role="cht4Q" to="c8s6:4vv0wgO7Ca8" resolve="TerminalDeclaration" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1LlUBW" id="3v$U6ONP9Di" role="2ZBHrp">
          <node concept="3Tqbb2" id="3v$U6ONP9Lh" role="1Lm7xW">
            <ref role="ehGHo" to="c8s6:4vv0wgO8jGD" resolve="NonterminalInstance" />
          </node>
          <node concept="3Tqbb2" id="3v$U6ONP9UJ" role="1Lm7xW">
            <ref role="ehGHo" to="c8s6:3v$U6ONMnP1" resolve="Transform" />
          </node>
        </node>
      </node>
      <node concept="3cWJ9i" id="3v$U6ONOPFI" role="1Qtc8$">
        <node concept="CtIbL" id="3v$U6ONOPFK" role="CtIbM">
          <property role="CtIbK" value="30NnNOohrQL/RIGHT" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="7DEtAv3oMcc">
    <property role="3GE5qa" value="transform" />
    <ref role="1XX52x" to="c8s6:7DEtAv3oLdr" resolve="TransformPatternRule" />
    <node concept="3EZMnI" id="7DEtAv3oMcD" role="2wV5jI">
      <node concept="1iCGBv" id="7DEtAv3oMcn" role="3EZMnx">
        <ref role="1NtTu8" to="c8s6:7DEtAv3oLdy" resolve="rule" />
        <node concept="1sVBvm" id="7DEtAv3oMcr" role="1sWHZn">
          <node concept="3F0A7n" id="7DEtAv3oMcA" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="7DEtAv3oMcP" role="3EZMnx">
        <property role="3F0ifm" value="(" />
        <ref role="1k5W1q" to="tpen:hY9fg1G" resolve="LeftParenAfterName" />
      </node>
      <node concept="3F2HdR" id="7DEtAv3oMdf" role="3EZMnx">
        <property role="2czwfO" value=" " />
        <ref role="1NtTu8" to="c8s6:7DEtAv3oLd$" resolve="childPatterns" />
        <node concept="l2Vlx" id="7DEtAv3oMdh" role="2czzBx" />
      </node>
      <node concept="3F0ifn" id="7DEtAv3oMd1" role="3EZMnx">
        <property role="3F0ifm" value=")" />
        <ref role="1k5W1q" to="tpen:hFCSUmN" resolve="RightParen" />
      </node>
      <node concept="l2Vlx" id="7DEtAv3oMcE" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="7DEtAv3oMdM">
    <property role="3GE5qa" value="transform" />
    <ref role="1XX52x" to="c8s6:7DEtAv3oLdB" resolve="TransformPatternAny" />
    <node concept="3F0ifn" id="7DEtAv3oMdO" role="2wV5jI">
      <property role="3F0ifm" value="_" />
    </node>
  </node>
  <node concept="3p36aQ" id="7DEtAv41AZe">
    <property role="3GE5qa" value="transform" />
    <ref role="aqKnT" to="c8s6:3v$U6ONMnQB" resolve="TransformationsTable" />
    <node concept="3eGOop" id="7DEtAv41AZk" role="3ft7WO">
      <node concept="ucgPf" id="7DEtAv41AZl" role="3aKz83">
        <node concept="3clFbS" id="7DEtAv41AZm" role="2VODD2">
          <node concept="3clFbF" id="7DEtAv41B1N" role="3cqZAp">
            <node concept="2ShNRf" id="7DEtAv41B1L" role="3clFbG">
              <node concept="3zrR0B" id="7DEtAv41B9x" role="2ShVmc">
                <node concept="3Tqbb2" id="7DEtAv41B9z" role="3zrR0E">
                  <ref role="ehGHo" to="c8s6:3v$U6ONMnQB" resolve="TransformationsTable" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="7DEtAv41Bdh" role="upBLP">
        <node concept="2h3Zct" id="7DEtAv41BdR" role="16NeZM">
          <property role="2h4Kg1" value="manually described" />
        </node>
      </node>
      <node concept="16NL0t" id="7DEtAv41Bew" role="upBLP">
        <node concept="2h3Zct" id="7DEtAv41Bhb" role="16NL0q">
          <property role="2h4Kg1" value="transformations" />
        </node>
      </node>
    </node>
    <node concept="3eGOop" id="7DEtAv41Bhe" role="3ft7WO">
      <node concept="16NL3D" id="7DEtAv41CFu" role="upBLP">
        <node concept="16Na2f" id="7DEtAv41CFw" role="16NL3A">
          <node concept="3clFbS" id="7DEtAv41CFy" role="2VODD2">
            <node concept="3clFbF" id="7DEtAv41CKm" role="3cqZAp">
              <node concept="1Wc70l" id="7DEtAv42111" role="3clFbG">
                <node concept="2OqwBi" id="7DEtAv41FUY" role="3uHU7B">
                  <node concept="2OqwBi" id="7DEtAv41ErM" role="2Oq$k0">
                    <node concept="2OqwBi" id="7DEtAv41CZq" role="2Oq$k0">
                      <node concept="1yR$tW" id="7DEtAv41CKl" role="2Oq$k0" />
                      <node concept="2Ttrtt" id="7DEtAv41Ddg" role="2OqNvi" />
                    </node>
                    <node concept="v3k3i" id="7DEtAv41FzZ" role="2OqNvi">
                      <node concept="chp4Y" id="7DEtAv41F_f" role="v3oSu">
                        <ref role="cht4Q" to="c8s6:3v$U6ONMnQB" resolve="TransformationsTable" />
                      </node>
                    </node>
                  </node>
                  <node concept="3GX2aA" id="7DEtAv41G7b" role="2OqNvi" />
                </node>
                <node concept="2OqwBi" id="7DEtAv4216R" role="3uHU7w">
                  <node concept="2OqwBi" id="7DEtAv421_B" role="2Oq$k0">
                    <node concept="2OqwBi" id="7DEtAv4216S" role="2Oq$k0">
                      <node concept="2OqwBi" id="7DEtAv4216T" role="2Oq$k0">
                        <node concept="1yR$tW" id="7DEtAv4216U" role="2Oq$k0" />
                        <node concept="2Ttrtt" id="7DEtAv4216V" role="2OqNvi" />
                      </node>
                      <node concept="v3k3i" id="7DEtAv4216W" role="2OqNvi">
                        <node concept="chp4Y" id="7DEtAv421hc" role="v3oSu">
                          <ref role="cht4Q" to="c8s6:4vv0wgOacKR" resolve="LL1Table" />
                        </node>
                      </node>
                    </node>
                    <node concept="3zZkjj" id="7DEtAv421Qf" role="2OqNvi">
                      <node concept="1bVj0M" id="7DEtAv421Qh" role="23t8la">
                        <node concept="3clFbS" id="7DEtAv421Qi" role="1bW5cS">
                          <node concept="3clFbF" id="7DEtAv421VV" role="3cqZAp">
                            <node concept="17R0WA" id="7DEtAv423JX" role="3clFbG">
                              <node concept="Xl_RD" id="7DEtAv423V3" role="3uHU7w">
                                <property role="Xl_RC" value="reduced: augmented grammar" />
                              </node>
                              <node concept="2OqwBi" id="7DEtAv422hd" role="3uHU7B">
                                <node concept="37vLTw" id="7DEtAv421VU" role="2Oq$k0">
                                  <ref role="3cqZAo" node="7DEtAv421Qj" resolve="it" />
                                </node>
                                <node concept="3TrcHB" id="7DEtAv422n3" role="2OqNvi">
                                  <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="Rh6nW" id="7DEtAv421Qj" role="1bW2Oz">
                          <property role="TrG5h" value="it" />
                          <node concept="2jxLKc" id="7DEtAv421Qk" role="1tU5fm" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3GX2aA" id="7DEtAv4216Y" role="2OqNvi" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="ucgPf" id="7DEtAv41Bhf" role="3aKz83">
        <node concept="3clFbS" id="7DEtAv41Bhg" role="2VODD2">
          <node concept="3cpWs8" id="7DEtAv42e5R" role="3cqZAp">
            <node concept="3cpWsn" id="7DEtAv42e5S" role="3cpWs9">
              <property role="TrG5h" value="baseTransforms" />
              <node concept="3Tqbb2" id="7DEtAv42dTw" role="1tU5fm">
                <ref role="ehGHo" to="c8s6:3v$U6ONMnQB" resolve="TransformationsTable" />
              </node>
              <node concept="2OqwBi" id="7DEtAv42e5T" role="33vP2m">
                <node concept="2OqwBi" id="7DEtAv42e5U" role="2Oq$k0">
                  <node concept="2OqwBi" id="7DEtAv42e5V" role="2Oq$k0">
                    <node concept="1yR$tW" id="7DEtAv42e5W" role="2Oq$k0" />
                    <node concept="2Ttrtt" id="7DEtAv42e5X" role="2OqNvi" />
                  </node>
                  <node concept="v3k3i" id="7DEtAv42e5Y" role="2OqNvi">
                    <node concept="chp4Y" id="7DEtAv42e5Z" role="v3oSu">
                      <ref role="cht4Q" to="c8s6:3v$U6ONMnQB" resolve="TransformationsTable" />
                    </node>
                  </node>
                </node>
                <node concept="1uHKPH" id="7DEtAv42e60" role="2OqNvi" />
              </node>
            </node>
          </node>
          <node concept="3cpWs8" id="7DEtAv425nJ" role="3cqZAp">
            <node concept="3cpWsn" id="7DEtAv425nK" role="3cpWs9">
              <property role="TrG5h" value="llTable" />
              <node concept="3Tqbb2" id="7DEtAv425mx" role="1tU5fm">
                <ref role="ehGHo" to="c8s6:4vv0wgOacKR" resolve="LL1Table" />
              </node>
              <node concept="2OqwBi" id="7DEtAv425nL" role="33vP2m">
                <node concept="2OqwBi" id="7DEtAv425nM" role="2Oq$k0">
                  <node concept="2OqwBi" id="7DEtAv425nN" role="2Oq$k0">
                    <node concept="2OqwBi" id="7DEtAv425nO" role="2Oq$k0">
                      <node concept="1yR$tW" id="7DEtAv425nP" role="2Oq$k0" />
                      <node concept="2Ttrtt" id="7DEtAv425nQ" role="2OqNvi" />
                    </node>
                    <node concept="v3k3i" id="7DEtAv425nR" role="2OqNvi">
                      <node concept="chp4Y" id="7DEtAv425nS" role="v3oSu">
                        <ref role="cht4Q" to="c8s6:4vv0wgOacKR" resolve="LL1Table" />
                      </node>
                    </node>
                  </node>
                  <node concept="3zZkjj" id="7DEtAv425nT" role="2OqNvi">
                    <node concept="1bVj0M" id="7DEtAv425nU" role="23t8la">
                      <node concept="3clFbS" id="7DEtAv425nV" role="1bW5cS">
                        <node concept="3clFbF" id="7DEtAv425nW" role="3cqZAp">
                          <node concept="17R0WA" id="7DEtAv425nX" role="3clFbG">
                            <node concept="Xl_RD" id="7DEtAv425nY" role="3uHU7w">
                              <property role="Xl_RC" value="reduced: augmented grammar" />
                            </node>
                            <node concept="2OqwBi" id="7DEtAv425nZ" role="3uHU7B">
                              <node concept="37vLTw" id="7DEtAv425o0" role="2Oq$k0">
                                <ref role="3cqZAo" node="7DEtAv425o2" resolve="it" />
                              </node>
                              <node concept="3TrcHB" id="7DEtAv425o1" role="2OqNvi">
                                <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="Rh6nW" id="7DEtAv425o2" role="1bW2Oz">
                        <property role="TrG5h" value="it" />
                        <node concept="2jxLKc" id="7DEtAv425o3" role="1tU5fm" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="1uHKPH" id="7DEtAv425o4" role="2OqNvi" />
              </node>
            </node>
          </node>
          <node concept="3cpWs8" id="7DEtAv41BkQ" role="3cqZAp">
            <node concept="3cpWsn" id="7DEtAv41BkR" role="3cpWs9">
              <property role="TrG5h" value="result" />
              <node concept="3Tqbb2" id="7DEtAv41Bkk" role="1tU5fm">
                <ref role="ehGHo" to="c8s6:3v$U6ONMnQB" resolve="TransformationsTable" />
              </node>
              <node concept="2ShNRf" id="7DEtAv41BkS" role="33vP2m">
                <node concept="3zrR0B" id="7DEtAv41BkT" role="2ShVmc">
                  <node concept="3Tqbb2" id="7DEtAv41BkU" role="3zrR0E">
                    <ref role="ehGHo" to="c8s6:3v$U6ONMnQB" resolve="TransformationsTable" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="7DEtAv41Bhh" role="3cqZAp">
            <node concept="37vLTI" id="7DEtAv41CcX" role="3clFbG">
              <node concept="3cpWs3" id="7DEtAv41C_t" role="37vLTx">
                <node concept="Xl_RD" id="7DEtAv41CdQ" role="3uHU7B">
                  <property role="Xl_RC" value="expanded: " />
                </node>
                <node concept="2OqwBi" id="7DEtAv41Ofn" role="3uHU7w">
                  <node concept="37vLTw" id="7DEtAv42e61" role="2Oq$k0">
                    <ref role="3cqZAo" node="7DEtAv42e5S" resolve="baseTransforms" />
                  </node>
                  <node concept="3TrcHB" id="7DEtAv41O_n" role="2OqNvi">
                    <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="7DEtAv41BxC" role="37vLTJ">
                <node concept="37vLTw" id="7DEtAv41BkV" role="2Oq$k0">
                  <ref role="3cqZAo" node="7DEtAv41BkR" resolve="result" />
                </node>
                <node concept="3TrcHB" id="7DEtAv41BJt" role="2OqNvi">
                  <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="7DEtAv427Gn" role="3cqZAp">
            <node concept="2OqwBi" id="7DEtAv429Y7" role="3clFbG">
              <node concept="2OqwBi" id="7DEtAv4282W" role="2Oq$k0">
                <node concept="37vLTw" id="7DEtAv427Gl" role="2Oq$k0">
                  <ref role="3cqZAo" node="7DEtAv41BkR" resolve="result" />
                </node>
                <node concept="3Tsc0h" id="7DEtAv428$o" role="2OqNvi">
                  <ref role="3TtcxE" to="c8s6:3v$U6ONMnQE" resolve="transformations" />
                </node>
              </node>
              <node concept="X8dFx" id="7DEtAv42byY" role="2OqNvi">
                <node concept="2OqwBi" id="7DEtAv437pc" role="25WWJ7">
                  <node concept="2OqwBi" id="7DEtAv42pYh" role="2Oq$k0">
                    <node concept="2OqwBi" id="7DEtAv42nkP" role="2Oq$k0">
                      <node concept="37vLTw" id="7DEtAv447dY" role="2Oq$k0">
                        <ref role="3cqZAo" node="7DEtAv42e5S" resolve="baseTransforms" />
                      </node>
                      <node concept="3Tsc0h" id="7DEtAv42nTw" role="2OqNvi">
                        <ref role="3TtcxE" to="c8s6:3v$U6ONMnQE" resolve="transformations" />
                      </node>
                    </node>
                    <node concept="3zZkjj" id="7DEtAv42ubH" role="2OqNvi">
                      <node concept="1bVj0M" id="7DEtAv42ubJ" role="23t8la">
                        <node concept="3clFbS" id="7DEtAv42ubK" role="1bW5cS">
                          <node concept="3clFbF" id="7DEtAv42vIx" role="3cqZAp">
                            <node concept="1Wc70l" id="7DEtAv42GKC" role="3clFbG">
                              <node concept="2OqwBi" id="7DEtAv434o9" role="3uHU7w">
                                <node concept="2OqwBi" id="7DEtAv42TTG" role="2Oq$k0">
                                  <node concept="2OqwBi" id="7DEtAv42NMS" role="2Oq$k0">
                                    <node concept="2OqwBi" id="7DEtAv42JMz" role="2Oq$k0">
                                      <node concept="37vLTw" id="7DEtAv42HOt" role="2Oq$k0">
                                        <ref role="3cqZAo" node="7DEtAv42ubL" resolve="it" />
                                      </node>
                                      <node concept="3TrEf2" id="7DEtAv42Mjr" role="2OqNvi">
                                        <ref role="3Tt5mk" to="c8s6:7DEtAv3ysTw" resolve="toPattern" />
                                      </node>
                                    </node>
                                    <node concept="2qgKlT" id="7DEtAv42ON3" role="2OqNvi">
                                      <ref role="37wK5l" to="h9ya:7DEtAv3qu1D" resolve="getConsequence" />
                                      <node concept="2OqwBi" id="7DEtAv42Q0D" role="37wK5m">
                                        <node concept="37vLTw" id="7DEtAv42PsB" role="2Oq$k0">
                                          <ref role="3cqZAo" node="7DEtAv42ubL" resolve="it" />
                                        </node>
                                        <node concept="2qgKlT" id="7DEtAv42QVN" role="2OqNvi">
                                          <ref role="37wK5l" to="h9ya:7DEtAv3xC4_" resolve="getSymbol" />
                                        </node>
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="1yVyf7" id="7DEtAv42XU3" role="2OqNvi" />
                                </node>
                                <node concept="1mIQ4w" id="7DEtAv435P0" role="2OqNvi">
                                  <node concept="chp4Y" id="7DEtAv436lX" role="cj9EA">
                                    <ref role="cht4Q" to="c8s6:4vv0wgO7Ca9" resolve="NonterminalDeclaration" />
                                  </node>
                                </node>
                              </node>
                              <node concept="2OqwBi" id="7DEtAv42C9C" role="3uHU7B">
                                <node concept="2OqwBi" id="7DEtAv42wFb" role="2Oq$k0">
                                  <node concept="37vLTw" id="7DEtAv42vIw" role="2Oq$k0">
                                    <ref role="3cqZAo" node="7DEtAv42ubL" resolve="it" />
                                  </node>
                                  <node concept="3TrcHB" id="7DEtAv42zzC" role="2OqNvi">
                                    <ref role="3TsBF5" to="c8s6:7DEtAv3lMVa" resolve="side" />
                                  </node>
                                </node>
                                <node concept="21noJN" id="7DEtAv42DFL" role="2OqNvi">
                                  <node concept="21nZrQ" id="7DEtAv42E4g" role="21noJM">
                                    <ref role="21nZrZ" to="tpc2:3Ftr4R6BFex" resolve="RIGHT" />
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="Rh6nW" id="7DEtAv42ubL" role="1bW2Oz">
                          <property role="TrG5h" value="it" />
                          <node concept="2jxLKc" id="7DEtAv42ubM" role="1tU5fm" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3$u5V9" id="7DEtAv439XK" role="2OqNvi">
                    <node concept="1bVj0M" id="7DEtAv439XM" role="23t8la">
                      <node concept="3clFbS" id="7DEtAv439XN" role="1bW5cS">
                        <node concept="3clFbF" id="7DEtAv43bv6" role="3cqZAp">
                          <node concept="2OqwBi" id="7DEtAv43e3q" role="3clFbG">
                            <node concept="37vLTw" id="7DEtAv43bv5" role="2Oq$k0">
                              <ref role="3cqZAo" node="7DEtAv439XO" resolve="it" />
                            </node>
                            <node concept="1$rogu" id="7DEtAv43gyq" role="2OqNvi" />
                          </node>
                        </node>
                      </node>
                      <node concept="Rh6nW" id="7DEtAv439XO" role="1bW2Oz">
                        <property role="TrG5h" value="it" />
                        <node concept="2jxLKc" id="7DEtAv439XP" role="1tU5fm" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="7DEtAv41ZDA" role="3cqZAp">
            <node concept="2OqwBi" id="7DEtAv41ZZS" role="3clFbG">
              <node concept="37vLTw" id="7DEtAv41ZD$" role="2Oq$k0">
                <ref role="3cqZAo" node="7DEtAv41BkR" resolve="result" />
              </node>
              <node concept="2qgKlT" id="7DEtAv420oV" role="2OqNvi">
                <ref role="37wK5l" to="h9ya:7DEtAv41OVb" resolve="expand" />
                <node concept="37vLTw" id="7DEtAv426q9" role="37wK5m">
                  <ref role="3cqZAo" node="7DEtAv425nK" resolve="llTable" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3cpWs6" id="7DEtAv41OGe" role="3cqZAp">
            <node concept="37vLTw" id="7DEtAv41OIL" role="3cqZAk">
              <ref role="3cqZAo" node="7DEtAv41BkR" resolve="result" />
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="7DEtAv41Bhl" role="upBLP">
        <node concept="2h3Zct" id="7DEtAv41Bhm" role="16NeZM">
          <property role="2h4Kg1" value="expanded" />
        </node>
      </node>
      <node concept="16NL0t" id="7DEtAv41Bhn" role="upBLP">
        <node concept="2h3Zct" id="7DEtAv41Bho" role="16NL0q">
          <property role="2h4Kg1" value="transformations" />
        </node>
      </node>
    </node>
    <node concept="3VyMlK" id="7DEtAv41BcA" role="3ft7WO" />
  </node>
</model>

